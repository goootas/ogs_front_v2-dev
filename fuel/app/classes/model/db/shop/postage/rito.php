<?php
class Model_Db_Shop_Postage_Rito extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'zip',
		'price',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_shop_postage_rito';

	public static function validate($factory)
    {
        $val = Validation::forge($factory);
        return $val;
    }

	public static function findPriceListTool($where="")
	{
		$data = static::find('all', array(
			'where'	=> $where,
			)
		);
		return $data;
	}
	public static function findPriceDetailTool($where="")
	{
		$data = static::find('first', array(
			'where'	=> $where,
			)
		);
		return $data;
	}

}

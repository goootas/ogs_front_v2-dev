<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Language" content="ja">
	<meta charset="utf-8">
	<?php if(preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) { ?>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<?php } else { ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php } ?>
	<meta name="description" content="<?php echo $shop_data["description"];?>">
	<meta name="keywords" content="<?php echo $shop_data["keywords"];?>">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="/<?php echo $shop_data["dir"];?>/assets/icon/gackt.ico">
	<title><?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?></title>
	<?php
	echo Asset::css(
		array(
			Config::get("assets_url").'css/reset.css',
			Config::get("assets_url").'css/common.css',
//			Config::get("assets_url").'css/'.$shop_data["dir"].'/'.$shop_data["dir"].'.css',
			$shop_data["dir"].'/'.$shop_data["dir"].'.css',
			Config::get("assets_url").'css/validationEngine.jquery.min.css',
		)
	);
	if(preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
		echo Asset::css(
			array(
				Config::get("assets_url").'css/common-ie.css',
				Config::get("assets_url").'css/'.$shop_data["dir"].'/common-ie.css',
			)
		);
	}
	echo Asset::js(
		array(
			Config::get("assets_url").'js/jquery.1.8.2.min.js',
			Config::get("assets_url").'js/common.js',
//			Config::get("assets_url").'js/jquery.validationEngine-ja.min.js',
			"jquery.validationEngine-ja.min.js",
			Config::get("assets_url").'js/jquery.validationEngine.min.js',
//			'jquery.validationEngine.js',
		)
	);
	if(!preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
		echo Asset::js(
			array(
				Config::get("assets_url").'js/jquery.innerfade.js',
				Config::get("assets_url").'js/masonry.pkgd.min.js',
				Config::get("assets_url").'js/jquery.autopager-1.0.0.js'
			)
		);
	}
	?>
	<style>
		<!--
		.form-group input[type="tel"],.form-group input[type="email"],
		.form-group input[type="text"], .form-group input[type="password"], .form-group textarea {
			background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
			padding: 12px;
			width: 90%;
		}
		/*.box .text dl {*/
			/*padding: 15px 10px 15px 10px;*/
		/*}*/
		-->
	</style>

	<?php //TODO:OGタグの内容確認お願いします。 ?>
	<?php if(Request::active()->controller == 'Controller_Product' and in_array(Request::active()->action, array('detail'))) :?>
		<meta property="fb:app_id" content="<?php echo $shop_data["description"];?>"/>
		<meta property="og:title" content="<?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?php echo $shop_data["description"];?>"/>
		<meta property="og:url" content="https://<?php echo $_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI'] . "?" . $_SERVER['QUERY_STRING']; ?>"/>
		<meta property="og:image" content="<?php echo "https://" . $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/" . Config::get("aws.image_path") . $product->id . "/" . $product->id . "_000.jpg"; ?>"/>
		<meta property="og:site_name" content="<?php echo $shop_data["name"] ?>"/>
	<?php endif; ?>

</head>
<body>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', '<?php echo $shop_data["ga_tag"]?>', 'auto');
	ga('require', 'linkid', 'linkid.js');
	ga('send', 'pageview');

</script>
<div id="" class="WRAPPER">
	<div class="HEADER MIN">

		<div class="MAINTITLE">
			<div class="main_visual"><?php echo Asset::img($shop_data["dir"].'/LVL_logo.png', array('class' => 'visual')); ?><a href='/<?php echo $shop_data["dir"];  ?>'><?php echo Asset::img($shop_data["dir"].'/LVL_LOGO_2.png', array('class' => 'evelogo')); ?><h2><?php echo Asset::img($shop_data["dir"].'/goods_logo_gold.png', array('class' => 'goodslogo')); ?></h2></a></div>
			<div class="TITLE">
				<?php if(Request::active()->controller == 'Controller_Top') :?>
				<?php else : ?>
				<?php endif; ?>
				<?php echo isset($shop_desc->pcsp_top1) ? $shop_desc->pcsp_top1 : "";?>
			</div>
		</div>
		<div class="NAVIGATION">
			<div class="GLOBAL">
				<div class="CONTAINER">
					<div class="buttons">
						<?php if(Session::get('user.id')) : ?>
							<?php echo Html::anchor('mypage', '<span>マイページ</span>', array("class" => "button text with-icon mypage"), true); ?>
							<?php echo Html::anchor('logout', '<span>ログアウト</span>', array("class" => "button text with-icon logout"), true); ?>
						<?php else: ?>
							<?php echo Html::anchor('login', '<span>LOGIN</span>', array("class" => "button text with-icon login"), true); ?>
						<?php endif; ?>
						<?php echo Html::anchor('cart', '<span>MYCART</span>',array("class" => "button key rect with-icon cart")); ?>
					</div>
				</div>
			</div>
            	<div class="CATE">
				<div class="CONTAINER">
					<div class="buttons"><?php echo Html::anchor('/', "<span style='font-weight: bold;'>ALL</span>", array("class" => "button text all"), true); ?></div>
					<div class="buttons"> <a class="button text categories" href="#"><span style='font-weight: bold;'>カテゴリー一覧</span></a> </div>
					<div class="navigation">
						<ul >
							<?php if($category1): ?>
								<?php foreach($category1 as $c1): ?>
									<li><?php echo Html::anchor('/product/list?c1='.$c1->id, "<span style='font-weight: bold;'>".$c1->title."</span>") ?></li>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php if(Request::active()->controller == 'Controller_Top') :?>
		<?php if($banner): ?>
			<?php foreach($banner as $b): ?>
				<?php
				$img_path = "https:".Config::get("banner_url").$b->id.'_pc.jpg';
				$array = get_headers($img_path);
				if($b->sort == 1){
					if(strpos($array[0],'OK')){
						?>
						<div class="SPECIAL">
							<?php if($b->url == ""){?>
								<?php echo Asset::img($img_path, array('class' => 'banner','alt' => $b->title,'id' => 'logo'));?>
							<?php }else{?>
								<a href="<?php echo $b->url;?>"><?php echo Asset::img($img_path, array('class' => 'banner','alt' => $b->title,'id' => 'logo'));?></a>
							<?php }?>
						</div>
					<?php } ?>
				<?php } ?>
			<?php endforeach; ?>
		<?php endif; ?>
	<?php endif; ?>

	<div class="CONTENTS">
		<div class="CONTAINER">
			<?php echo $content; ?>
		</div>
	</div>

	<?php if(Request::active()->controller == 'Controller_Top') :?>
		<?php if($banner): ?>
			<?php foreach($banner as $b): ?>
				<?php
				$img_path = "https:".Config::get("banner_url").$b->id.'_pc.jpg';
				$array = get_headers($img_path);
				if($b->sort == 2){
					if(strpos($array[0],'OK')){
						?>
						<div class="SPECIAL">
							<?php if($b->url == ""){?>
								<?php echo Asset::img($img_path, array('class' => 'banner','alt' => $b->title,'id' => 'logo'));?>
							<?php }else{?>
								<a href="<?php echo $b->url;?>"><?php echo Asset::img($img_path, array('class' => 'banner','alt' => $b->title,'id' => 'logo'));?></a>
							<?php }?>
						</div>
					<?php } ?>
				<?php } ?>
			<?php endforeach; ?>
		<?php endif; ?>
	<?php endif; ?>

	<div class="FOOTER">
		<?php echo isset($shop_desc->pcsp_top2) ? $shop_desc->pcsp_top2 : "";?>
		<div class="LINKS">
			<div class="CONTAINER">
				<div class="navigation">
					<ul>
						<li><?php echo Html::anchor('/info/guide', '<span>ショッピングガイド</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/info/privacy', '<span>プライバシーポリシー</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/info/legal', '<span>特定商取引法に基づく表記</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/inquiry', '<span>お問い合わせ</span>', "", true) ?></li>
						<li><?php echo Html::anchor('https://www.gackt.com/', '<span>GACKT OFFICIAL WEB</span>',  array("target" => "_blank")) ?></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="CREDIT">
			<div class="CONTAINER">
				<p>Copyright (c) <?php echo date("Y");?> <?php echo $shop_data["copyright"] ? $shop_data["copyright"] : "RENI Co.,Ltd.";?> All Rights Reserved</p>
			</div>
		</div>
	</div>
</div>
</body>
</html>
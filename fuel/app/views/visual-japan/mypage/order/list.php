<?php if ($order): ?>
	<div class="row">
		<table class="table table-bordered vert-align">
			<tr>
				<th>注文番号</th>
				<th>注文日時</th>
				<th>配達日時</th>
				<th>キャンセル日時</th>
				<th>お届け先氏名</th>
				<th>決済</th>
			</tr>
			<?php foreach ($order as $data): ?>
				<tr>
					<td><?php echo $data["order_id"];?></td>
					<td><?php echo $data["insert_date"];?></td>
					<td><?php echo $data["delivery_date"] ? $data["delivery_date"] : "配達準備中";?></td>
					<td><?php echo $data["cancel_date"] ?  $data["cancel_date"] : "キャンセル";?></td>
					<td><?php echo $data["deliver_username_sei"].$data["deliver_username_mei"];?></td>
					<td><?php echo Config::get("payment.status.".$data["payment"]);?></td>
				</tr>
				<tr>
					<td colspan="6">
						<table class="table table-bordered vert-align">
							<tr>
								<th>商品番号</th>
								<th>商品名</th>
								<th>数量</th>
							</tr>
							<?php foreach ($data["detail"] as $detail): ?>
								<tr>
									<td><?php echo $detail["product"]["base"]["code"];?></td>
									<td>
										<?php echo $detail["product"]["base"]["title"];?>
										<?php echo $detail["product"]["option1_name"];?>
										<?php echo $detail["product"]["option2_name"];?>
									</td>
									<td><?php echo $detail["num"];?></td>
								</tr>
							<?php endforeach; ?>
						</table>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
	</div>
<?php else: ?>
	<p>注文はございません。</p>
<?php endif; ?>


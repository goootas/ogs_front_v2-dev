<?php
use Model\lib\Image;
use Fuel\Core\Config;
/*
 * TOPページ
 */
class Controller_Top extends Controller_Basefront
{
	public function action_list($page=0)
	{
		self::action_index($page);
	}
	public function action_index($page=0)
	{
		//----------------------------------
		// TOPIC情報取得 CHETのみで利用
		//----------------------------------
		if($this->shop_data["dir"] == "chet"){
			$wheres["shop_id"] = $this->shop_data["id"];
			$topic_list = Model_Db_Topic::findTopicList($wheres,3);
			$this->template->set_global('topics', $topic_list, false);
		}

		//----------------------------------
		// 販売期間情報取得
		//----------------------------------
		$term_ids = array();
		foreach($this->term_data as $val){
			$term_ids[] = $val->id;
		}

		//----------------------------------
		// 商品情報取得
		//----------------------------------
		$wheres["shop_id"] = $this->shop_data["id"];
		$wheres["term_ids"] = implode(",", $term_ids);

		// APBANK専用対応
		if($this->shop_data["dir"] == "apbankfes2016") {
			$wheres["c1"] = Config::get("apbankfes2016_category1");
		}

		\Fuel\Core\Log::debug(print_r(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>",true));
		\Fuel\Core\Log::debug(print_r("top_product_total_".$this->shop_data["id"],true));
		try
		{
			$product_total = Cache::get("top_product_total_".$this->shop_data["id"]);
			\Fuel\Core\Log::debug("CACHE top_product_".print_r($product_total,true));
		}
		catch (\CacheNotFoundException $e)
		{
			$product_total = Model_Db_Product::findProductList($wheres);
			Cache::set("top_product_total_".$this->shop_data["id"], $product_total, 60 * 1);
			\Fuel\Core\Log::debug("DB top_product_".print_r($product_total,true));
		}

		$page_limit_num = 12;
		if($this->agent_dir !== ""){
			$page_limit_num = 6;
		}
		if($this->shop_data["dir"] == "chet") {
			$page_limit_num = 100;
		}

		$param = array(
			'pagination_url' => ($this->shop_data["type"] ? "/".$this->shop_data["dir"] : "") . '/top/list/',
			'total_items' => count($product_total),
			'per_page' => $page_limit_num,
			'uri_segment' => $this->shop_data["type"] ? 4 : 3,
			'current_page' => $page,
			'num_links' => 0,
		);

		// 最大件数がページング数に見たない場合は処理は行わない。
		$paginations = Pagination::forge('list', $param);
		if($page_limit_num > count($product_total)){
			$product_list = $product_total;
		}else{
			try
			{
				$product_list = Cache::get("top_product_list_".$this->shop_data["id"]."_".$page);
				\Fuel\Core\Log::debug("CACHE top_product_list".print_r($product_list,true));
			}
			catch (\CacheNotFoundException $e)
			{
				$product_list = Model_Db_Product::findProductList($wheres, $paginations);
				Cache::set("top_product_list_".$this->shop_data["id"]."_".$page, $product_list, 60 * 1);
				\Fuel\Core\Log::debug("DB top_product_list".print_r($product_list,true));
			}
		}

		$this->template->set_global('paginations', $paginations->render(true), false);
		$this->template->set_global('total_pages', Pagination::instance('list')->total_pages, false);

		//----------------------------------
		// 商品画像
		//----------------------------------
		foreach($product_list as $key => $product){
			if($this->agent_dir == ""){
				$imgs = Image::getImage($product["id"]);
			}else{
				$imgs = Image::getImageFp($product["id"]);
			}
			$product_list[$key]["imgs"] = $imgs;
		}

		$this->template->set_global('products', $product_list, false);
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/top');
	}
}

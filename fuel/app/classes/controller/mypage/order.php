<?php
use Fuel\Core\Log;

/*
 * マイページ
 */
class Controller_Mypage_Order extends Controller_Basefront
{
	public function before()
	{
		// ログイン認証
		parent::before();
		is_null($this->user_data) and Response::redirect('/login');
		Config::load("veritrans");// ベリトランス設定ファイル読み込み
	}

	/*
	 * 購入履歴
	 */
	public function action_detail($order_id)
	{
		is_null($order_id) and Response::redirect('/mypage'.$this->session_get_param);

		// ---------------------------------------------------
		// 注文内容取得
		// ---------------------------------------------------
		$wheres = array();
		$wheres[] = array("shop_id", $this->shop_data["id"]);
		$wheres[] = array("user_id", $this->user_data->id);
		$wheres[] = array("order_id", $order_id);
		$order_data = Model_Db_Order::findListFront($wheres)->to_array();

		foreach($order_data["detail"] as $key => $base ){
			$wheres = array();
			$wheres[] = array("id", $base["did"]);
			$product = Model_Db_Product_Detail::findProductDetailOne($wheres)->to_array();
			$order_data["detail"][$key]["product"] = $product;
			$order_data["detail"][$key]["product"]["imgs_pc"] = \Model\lib\Image::getImage($product["base"]["id"]);
			$order_data["detail"][$key]["product"]["imgs_fp"] = \Model\lib\Image::getImageFp($product["base"]["id"]);

		}
		$this->template->set_global('order', $order_data, false);

		// ---------------------------------------------------
		// 名入れ情報取得
		// ---------------------------------------------------
		$arr_names = Model_Db_Order_Name::findMypageOrderList($order_id);
		$this->template->set_global('names', $arr_names, false);

		// ---------------------------------------------------
		// プリント情報取得
		// ---------------------------------------------------
		$arr_prints = Model_Db_Order_Print::findDetail($order_id);
		foreach ($arr_prints as $key => $print) {
			$arr_prints[$key]["body"] = Model_Db_Print::find($print["print_id"]);

		}
		$this->template->set_global('prints', $arr_prints, false);

		$print_product_name = "";
		$target_product_detail_ids = Config::get("print_product_detail_ids");
		if(isset($target_product_detail_ids)){
			foreach ($target_product_detail_ids as $target_product_detail_id) {
				$print_product_name = Model_Db_Product_Detail::findDetail($target_product_detail_id);
				$this->template->set_global('print_product_name', $print_product_name, false);
			}
		}

		// ---------------------------------------------------
		// 会場受渡情報取得receives
		// ---------------------------------------------------
		$arr_receives = Model_Db_Receives_Order::findMypageOrderList($order_id);
		$this->template->set_global('receives', $arr_receives, false);

		// ---------------------------------------------------
		// 楽天ID決済 注文番号取得
		// ---------------------------------------------------
		$rakuten_data = Model_Db_Rakuten::findDetail($order_id);
		$this->template->set_global('rakuten_data', $rakuten_data, false);
		Log::debug(print_r($rakuten_data,true));

		$this->template->title = "購入履歴";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/mypage/order/detail');
	}
}


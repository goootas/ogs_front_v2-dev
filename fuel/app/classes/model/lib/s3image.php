<?php
use Aws\S3\S3Client;

class Model_Lib_S3image extends \Model
{

    /*
     * ファイル一覧取得
     */
    public static function getList($id)
    {
        Config::load("common");
        try{
            $s3 = S3Client::factory(Config::get("aws"));
            $iterator = $s3->getIterator('ListObjects', array(
                'Bucket' => Config::get("aws.buckets"),
                'Prefix' => Config::get("aws.image_path").$id."/".$id,
            ));
            return $iterator;
        }catch (S3Exception $e) {
            //エラー処理
            Log::warning(print_r($e,true));
            return false;
        }

    }
    /*
     * ファイル一覧取得(ガラ用)
     */
    public static function getListFp($id)
    {
        Config::load("common");
        try{
            $s3 = S3Client::factory(Config::get("aws"));
            $iterator = $s3->getIterator('ListObjects', array(
                'Bucket' => Config::get("aws.buckets"),
                'Prefix' => Config::get("aws.image_path")."fp/".$id."/".$id,
            ));
            return $iterator;
        }catch (S3Exception $e) {
            //エラー処理
            Log::warning(print_r($e,true));
            return false;
        }

    }

    /*
     * ファイル登録/更新
     */
    public static function setFileSave($id,$save_file,$target_file)
    {
        try{
            Config::load("common");
            $s3 = S3Client::factory(Config::get("aws"));
            $s3->putObject(
                    array(
                        'Bucket'        =>   Config::get("aws.buckets"),
                        'Key'           =>   Config::get("aws.image_path").$id."/".$save_file,
                        'Body'          =>   fopen($target_file, 'r'),
                        'ACL'           =>   'public-read',
                        'ContentType'   =>   'image/jpeg',
                    )
                );
        }catch (S3Exception $e) {
            //エラー処理
            Log::warning(print_r($e,true));
            return false;
        }
        return true;
    }
    /*
     * ファイル論理削除
     */
    public static function setFileInvalid($id,$save_file)
    {

        try{
            Config::load("common");
            $s3 = S3Client::factory(Config::get("aws"));
            $s3->putObject(
                    array(
                        'Bucket'        =>   Config::get("aws.buckets"),
                        'Key'           =>   Config::get("aws.image_path").$id."/".$save_file,
                        'ACL'           =>   'private',
                    )
                );
        }catch (S3Exception $e) {
            //エラー処理
            Log::warning(print_r($e,true));
            return false;
        }
        return true;
    }
    /*
     * ファイル物理削除
     */
    public static function deleteFile($id,$save_file)
    {
        try{
            Config::load("common");
            $s3 = S3Client::factory(Config::get("aws"));
            $s3->deleteObject(
                    array(
                        'Bucket'        =>   Config::get("aws.buckets"),
                        'Key'           =>   Config::get("aws.image_path").$id."/".$save_file,
                    )
                );
        }catch (S3Exception $e) {
            //エラー処理
            Log::warning(print_r($e,true));
            return false;
        }
        return true;

    }


    //--------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------
    /*
	 * ファイルをリサイズ
	 */
    public static function resizeImage($file,$size = 0.5)
    {
        // 目的の画像から幅、高さを取得し,半分の値を計算します。
        list($width, $height) = getimagesize($file);
        $new_width = $width * $size;
        $new_height = $height * $size;

        $image_p = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefromjpeg($file);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

        // 出力
        $temp_file = tempnam(sys_get_temp_dir(), 'resize_');

        imagejpeg($image_p, $temp_file, 100);

        return $temp_file;

    }
}
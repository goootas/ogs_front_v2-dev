<?php
class Model_Db_Prefectures extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'region_id',
		'name',
		'sort',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'mst_prefectures';

	public static function validate($factory)
    {
        $val = Validation::forge($factory);

        return $val;
    }
	public static function findDetailAPI($where="")
	{
		$data = static::find('first', array(
			'where'		=> $where,
			)
		);
		return $data;
	}
}

<?php
class Model_Db_Object extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'image_url',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_object';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);

		return $val;
	}

	public static function findList($where="",$sort="",$paginations="")
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= " FROM " . self::$_table_name;
		$sql .= " WHERE 1 = 1 ";
		if(isset($where["shop_id"])){
			$sql .= "AND shop_id =".$where["shop_id"]." ";
		}
		if(isset($where["status"])){
			$sql .= "AND status =".$where["status"]." ";
		}
		$sql .= " ORDER BY insert_date desc ";

		if($paginations){
			$sql .= " LIMIT ".$paginations->per_page;
			$sql .= " OFFSET ".$paginations->offset;
		}

		$query = DB::query($sql);
		$data = $query->execute()->as_array();

		return $data;
	}
}

<?php // TODO: JQueryを使わないで処理する方法に変更　?>
<h4>パスワード再設定</h4>
<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<?php echo Form::open();?>
<div>
	新しいパスワードを入力して、設定ボタンを押してください。<br>
	<div>
		メールアドレス<br>
		<?php echo Session::get('forget.email'); ?>
	</div>
	<div>
		新しいパスワード<br>
		<?php echo Form::password('password'); ?>
	</div>
	<div>
		新しいパスワード(確認用)<br>
		<?php echo Form::password('password2'); ?>
	</div>

	<div>
		<?php echo Form::submit('exec', '設定'); ?><br>
	</div>
</div>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
<?php echo Form::close();?>

<style>
	.CONTENTS {
		margin-top: 0;
	}
	.CONTENTS .CONTAINER {
		max-width: 100%;
		padding: 0;
		width: 100%;
		border-radius: 0;
	}
	.outer{
		height: 100%;
		margin: 0 auto;
		max-width: 960px;
		overflow: hidden;
		padding: 0 10px;
	}
	.headline{
		margin: 80px 15px 40px;
	}
	.headline::before {
		background-color: #fff;
	}
	.headline::after {
		background-color: #fff;
	}
	@media screen and (max-width: 1024px) {
		.CONTENTS .CONTAINER {
			padding: 0 !important;
			width: auto;
		}
		.outer{
			padding:0px;
		}
	}
</style>
<?php if ($products): ?>
	<?php if(Request::active()->controller == 'Controller_Top') :?>
		<div class="MAINSLIDER">
			<div class="flexslider">
				<ul class="slides">
					<li><img src="https://s3-ap-northeast-1.amazonaws.com/reni-ec-pro/banner/63_pc.jpg" /></li>
					<li><a href="/keyakizaka46/product/list?c1=89"><img src="https://s3-ap-northeast-1.amazonaws.com/reni-ec-pro/banner/64_pc.jpg" /></a></li>
					<li><a href="/keyakizaka46/product/list?c1=88"><img src="https://s3-ap-northeast-1.amazonaws.com/reni-ec-pro/banner/65_pc.jpg" /></a></li>
					<li><a href="/keyakizaka46/product/list?c1=87"><img src="https://s3-ap-northeast-1.amazonaws.com/reni-ec-pro/banner/66_pc.jpg" /></a></li>
					<li><a href="/keyakizaka46/product/list?c1=86"><img src="https://s3-ap-northeast-1.amazonaws.com/reni-ec-pro/banner/67_pc.jpg" /></a></li>
					<li><a href="/keyakizaka46/product/list?c1=86"><img src="https://s3-ap-northeast-1.amazonaws.com/reni-ec-pro/banner/68_pc.jpg" /></a></li>
				</ul>
			</div>
		</div>
	<?php endif; ?>
	<div class="FREE-TEXT HEAD BUNNER">

		<?php if(Request::active()->controller == 'Controller_Top') :?>
			<?php echo Asset::img("https://reni-ec-pro.s3-ap-northeast-1.amazonaws.com/object/1/4/8/1/3/7/7/3/4/5/f7dc318bbde9190bc2f2b6366e24a6535a909564.gif", array('style' => "margin-bottom: 2px;"));?>
		<?php endif; ?>

		<?php if($banner): ?>
			<?php foreach($banner as $b): ?>
				<?php
				$img_path = "https:".Config::get("banner_url").$b->id.'_pc.jpg';
				$array = get_headers($img_path);
				if(strpos($array[0],'OK')){
					?>

					<?php if($b->url == ""){?>
						<?php echo Asset::img($img_path, array('alt' => $b->title,'style' => "margin-bottom: 2px;"));?>
					<?php }else{?>
						<a href="<?php echo $b->url;?>" ><?php echo Asset::img($img_path, array('alt' => $b->title , 'style' => 'width: 100%;'));?></a>
					<?php }?>

				<?php } ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>

	<div class="UP">
		<?php echo isset($shop_desc->pcsp_top1) ? $shop_desc->pcsp_top1 : "";?>
	</div>

	<div class="SP">
		<div class="NAVIGATION2">
			<div class="CATE2">
				<div class="CONTAINER2">
					<div class="buttons"> <a class="button text categories" href="javascript:void(0);"><span style='font-weight: bold;'>カテゴリーで探す</span></a> </div>
					<div class="navigation">
						<ul >
							<?php if($category1): ?>
								<?php foreach($category1 as $c1): ?>
									<li><?php echo Html::anchor('/product/list?c1='.$c1->id, "<span style='font-weight: bold;'>".$c1->title."</span>") ?></li>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>
					</div>
					<div class="buttons right">
						<?php echo Html::anchor('cart', '<span>CART</span>',array("class" => "button own with-icon cart")); ?>
					</div>
					<div class="buttons right">
						<?php if(Session::get('user.id')) : ?>
							<?php echo Html::anchor('mypage', '<span>MYPAGE</span>', array("class" => "button own with-icon mypage"), true); ?>
						<?php else: ?>
							<?php echo Html::anchor('login', '<span>LOGIN</span>', array("class" => "button own with-icon login"), true); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="outer">
		<!-- <h4 class="headline">ITEM</h4> -->
		<div class="marsonry">
			<ul>
				<?php foreach ($products as $key => $line): ?>
					<li class="box">
						<?php
						$html = '<div class="inner imgchange">';
						$html .= '<div class=""></div><div class="thumbnail">';
						if(isset($line["imgs"][0])){
//							$path_front = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0]."?".time();
//							$path_back = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0]."?".time();
							$path_front = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0];
							$path_back = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0];
						}else{
							$path_front = "";
							$path_back = "";
						}
						$img_tag2 = <<< EOM
<img src="//{$path_front}">
<!--<img src="//{$path_back}">-->
EOM;
						$html .= $img_tag2;
						$html .= '</div>';
						$html .= '<div class="text">';
						if($agent == "sp"){
							$html .= '<dl style="background: linear-gradient(to bottom, rgba(229,0,17,0) 0%,rgba(0, 0, 0, 0.59) 100%);">';
						}else{
							$html .= '<dl>';
						}
						$html .= '<dt>'.$line["title"].'</dt>';

						if($line["code"] == "KZ042" || $line["code"] == "KZ043"){
							$html .= '<dd>近日販売予定</dd>';
						}else{
							$html .= '<dd>¥'.number_format(floor($line["price_sale"] * $tax)).'</dd>';
						}
						$html .= '</dl>';
						$html .= '</div>';
						$html .= '</div>';
						?>
						<?php echo Html::anchor('/product/detail/'.$line["id"], $html) ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<?php if(Pagination::instance('list')->current_page != Pagination::instance('list')->total_pages): ?>
		<div id="NEXT" class="buttons">
			<?php echo Pagination::instance('list')->next(); ?>
			<?php echo Asset::img($shop_data["dir"].'/loader.gif', array('id' => 'loading','alt' => '読み込み中','width' => '29','height' => '29',));?>
		</div>
	<?php endif; ?>
<?php else: ?>
	<p>NO ITEM</p>
<?php endif; ?>

<script type="text/javascript">
	$(function($){
		$('.HEADER').removeClass('MIN');
	});
	$(document).on({
		"mouseenter": function(){
			$(this).find("img").attr("src",$(this).find("#out").val());
		},
		"mouseleave": function(){
			$(this).find("img").attr("src",$(this).find("#hover").val());
		}
	}, ".imgchange");

</script>
<?php if(!preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {?>
	<script type="text/javascript">
		$(function($){
			//タイルレイアウト実行
			function marsonryDo(){
				var windowW = $(window).width()
				var parentW = $(".CONTENTS .marsonry").width()
				if(windowW < 728){
					var boxW = (parentW) / 2
					var Hnum = 1.45
//					$('.box:nth-child(5n+1)').addClass("pickup");
				} else {
					var rand = Math.floor(Math.random()*2 + 1) ;
					var boxW = (parentW) / 3
//					$('.box:nth-child(' + rand + ')').addClass("pickup");
					var Hnum = 1.2
				}
				$('.box').each(function(i){
					if($(this).hasClass("pickup")){
						$(this).css({
							"width" : boxW * 2+ "px",
							"height" : boxW * 2 + "px"
						});
					} else {
						$(this).css({
							"width" : boxW + "px",
							"height" : boxW * Hnum + "px"
						});
					}
					var thumbNailSize = $(this).width()
					$(this).find(".inner").css({
						"width" : (thumbNailSize - 10) + "px",
						"height" : (thumbNailSize * Hnum - 10) + "px"
					});
				});
				clossFadeDo()
				setTimeout(function(){
					$('.marsonry ul').masonry({
						itemSelector: '.box',
						columnWidth: boxW,
						isFitWidth: false
					});
					$('.box').css({
						opacity: 1,
					});
				},100);
			}
			//クロスフェード
			function clossFadeDo(){
				setTimeout(function(){
					$('.box:odd .thumbnail').innerfade({
						speed: 500,
						timeout: 9000,
						type: 'random_start'
					});
				},6000);
				$('.box:nth-child(1) .thumbnail , .box:even .thumbnail').innerfade({
					speed: 500,
					timeout: 9000,
					type: 'random_start'
				});
			}

			function autoLoad(){
				var maxpage = <?php echo $total_pages ?>;
				$('#loading').css('display', 'none');
				$.autopager({
					content: '.marsonry ul',
					link: '#NEXT a',
					autoLoad: true,

					start: function(current, next){
						$('#loading').css('display', 'block');
						$('#NEXT a').css('display', 'none');
					},
					load: function(current, next){
						$('#loading').css('display', 'none');
						$('#NEXT a').css('display', 'block');
						if( current.page >= maxpage ){
							marsonryDo()//パネル実行
							$('#NEXT a').hide();
							return false;
						}else{
							marsonryDo()//パネル実行
						}
					}
				});
				$('#NEXT a').click(function(){
					$.autopager('load');
					return false;
				});
			}
			//読み込み直後実行
			$(window).load(function () {
				setTimeout(function() {
					marsonryDo()
				}, 1000);
				autoLoad()
				$('.flexslider').flexslider({
					animation: "slide",
					directionNav: true,
					prevText: "",
					nextText: ""
				});
			});
			//リサイズ時実行
			var timer = false;
			$(window).resize(function() {
				if (timer !== false) {
					clearTimeout(timer);
				}
				timer = setTimeout(function() {
					marsonryDo()
				}, 1000);
			});});
	</script>
<?php } ?>

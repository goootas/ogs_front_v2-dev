<?php
class Model_Db_Postalfull extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'biz',
		'biz_name',
		'town_code',
		'zip_code',
		'pref',
		'town',
		'block',
		'street',
		'update_date'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_postal_full';
	protected static $_primary_key = array('id');

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		return $val;
	}

	public static function getData($zip)
	{
		$sql = "select * from " . self::$_table_name . " where zip_code like '".$zip."%'";
		$query = DB::query($sql);
		$data = $query->execute()->as_array();
		return $data;
	}

}

<?php
class Model_Db_Banner extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'title',
		'image_url',
		'url',
		'start_date',
		'end_date',
		'sort',
		'status',
		'insert_date',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_banner';
	protected static $_has_one = array(
		'shop' => array(
			'key_from' => 'shop_id',
			'model_to' => 'Model_Db_Shop',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);

		return $val;
	}

	public static function findListFront($where="",$sort="")
	{
		$data = static::find('all', array(
				'where'		=> $where,
				'order_by'	=> $sort,
			)
		);
		return $data;
	}
}

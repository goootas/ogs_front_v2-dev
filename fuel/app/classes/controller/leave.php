<?php
/*
 * 退会
 */
class Controller_Leave extends Controller_Basefront
{
	public function before()
	{
		// ログイン認証
		parent::before();
		is_null(Session::get('user.id')) and Response::redirect('/login');
	}

	public function action_index()
	{
		// 退会注意メッセージ、同意チェックボックス表示
		// POST時、同意チェックを確認し、対象データをアップデート、セッション削除(ユーザーのみ)を行ない、TOPページへリダイレクト
		// ※ステータスは無効とする。
		$user = Model_Db_User::find(Session::get('user.id'));
		is_null($user) and Response::redirect('/login');
		if (Input::method() == 'POST')
		{

		}
		$this->template->title = '退会';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/leave/index');
	}

}


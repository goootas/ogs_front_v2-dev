<?php
/*
 * メールアドレス、SNSでログイン
 */
class Controller_Login extends Controller_Basefront
{
	public function before()
	{
		parent::before();
		!is_null($this->user_data) and Response::redirect("/".$this->session_get_param);
	}

	public function action_index()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Db_User::validate('login');
			if ($val->run())
			{
				$user = Model_Db_User::findEmailPassword($this->shop_data["id"],Input::post('email'),  Input::post('password'));
				\Fuel\Core\Log::debug(print_r($user,true));
				if(is_null($user)){
					// アカウント存在無し＆パスワード、メアド入力ミス
					Session::set_flash('error', 'メールアドレスまたは、パスワードを再度ご確認の上、ご入力ください。');
				}else{
					Session::set('user',array('id' => $user->id));
					Model_Db_User::setLastLogin($user->id);
					// au 端末のために一工夫
					$uri_data = explode("?", Input::server("HTTP_REFERER"));
					if(is_array($uri_data)){
						$uri = $uri_data[0];
					}else{
						$uri = $uri_data;
					}
					\Fuel\Core\Log::debug(print_r(str_replace(":443","",$uri).$this->session_get_param,true));

					Response::redirect(str_replace(":443","",$uri).$this->session_get_param);

				}
			}else{
				Session::set_flash('error', $val->error());
			}
		}
		$this->template->title = "ログイン";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/login/input');
	}

}





<h4 class="subheadline">FAQ</h4>
<div class="faq-container">
	<div class="head">注文内容を変更 / キャンセルしたい</div>
	<div class="body">「注文する」ボタン押下後の取り消し、お支払い方法の変更、ご注文の追加等、内容の変更は承ることができません。ご注文の際は十分にご検討のうえお申し込みください。</div>
</div>
<div class="faq-container">
	<div class="head">商品を交換 / 返品したい</div>
	<div class="body">不良品がお手元に届いてしまった場合、良品とお取り換えさせて頂きます。<br />
		（実際の商品の色が、画面上の色と微妙に異なることがございます）<br />
		返品は一切承っておりませんので、予めご了承下さい。<br />
		お届け商品が不良品だった場合、商品到着後７日以内にご連絡をお願い致します。商品到着後７日を過ぎてしまいますと、不良品交換はお受けできませんので、商品到着後に、なるべく早く内容のご確認をお願い致します。<br />
		お客様のもとで、破損、汚損が生じた場合には、お取り換え出来ません。<br />
		交換時に該当商品が品切れになった場合には、返金させて頂きますので、予めご了承下さい。</div>
</div>
<div class="faq-container">
	<div class="head">注文時に登録したメールアドレスを変更したい</div>
	<div class="body">メールアドレスはお客様ご自身にてマイページより変更していただくことが可能です。</div>
</div>
<div class="faq-container">
	<div class="head">お届け先を変更したい</div>
	<div class="body">ご注文後に住所等のご変更がございましたら、お問い合わせページよりご連絡ください。発送準備に入ってしまいますと、住所変更等を承ることができませんので、あらかじめご了承ください。</div>
</div>
<div class="faq-container">
	<div class="head">後払い決済について</div>
	<div class="body">請求書は商品とは別で送付いたしますので、ご確認ください。<br />
		<br />
		商品代金のお支払いは「コンビニ」「郵便局」「銀行」どこでも可能です。<br />
		請求書の記載事項に従って発行日から14日以内にお支払いください。<br />
		<br />
		○ご注意 後払い手数料：¥205<br />
		後払いのご注文には、株式会社キャッチボールよりの提供する後払い.comサービスが適用され、サービスの範囲内で個人情報を提供し、代金債権を譲渡します。
		<br />
		商品到着後2週間経過してもお支払用紙が届かない場合はお支払用紙が 届かない旨をご連絡くださいませ。
	</div>
</div>
<div class="faq-container">
	<div class="head">注文したがメールが届かない</div>
	<div class="body">注文完了画面に「受注番号」が表示されていれば、正常にご注文をお受けさせていただいております。 表示された受注番号にてマイページから内容をご確認ください。</div>
</div>


<h4 class="headline">お問い合わせ</h4>
<div class="forms">
	<?php echo Form::open(array('id' => 'inquiry' ,'autocomplete'=>'off'));?>
			<div class="form-group">
				<?php echo Form::label('お名前<span class="caption">※必須</span>', 'username',array("class" => "col-sm-3 control-label text-right")); ?>
				<div class="form-container">
				<div class="inner">
					<?php echo Form::input('username', Input::post('username', ''),
						array('class' => 'validate[required] form-control col-sm-4')); ?>
				</div>
				</div>
			</div>

			<div class="form-group">
				<?php echo Form::label('メールアドレス<span class="caption">※必須</span>', 'email',array("class" => "col-sm-3 control-label text-right")); ?>
				<div class="form-container">
				<div class="inner">
					<?php echo Form::input('email', Input::post('email', ''),
						array('class' => 'validate[required,custom[email]] form-control col-sm-4')); ?>
				</div>
				</div>
			</div>

			<div class="form-group">
				<?php echo Form::label('お問い合わせ内容<span class="caption">※必須</span>', 'message',array("class" => "col-sm-3 control-label text-right")); ?>
				<div class="form-container">
				<div class="inner">
					<?php echo Form::textarea('message', Input::post('message', ''),
						array('class' => 'validate[required] form-control','rows' =>'10')); ?>
				</div>
				</div>
			</div>


		<div class="buttons count-2">
			<?php echo Html::anchor('javascript:void(0);', '送信',array('id' => 'send' , 'class' => 'button rect key size-L'),true); ?>
		</div>

	<?php echo Form::close();?>
</div>
<script type="text/javascript">
	$(function(){
		$('.faq-container').find('.body').hide();
		$('.faq-container').click(function() {
			if (!$(this).hasClass("active")){
				$('.faq-container').removeClass("active");
				$('.faq-container').find('.body').slideUp(200);
				$(this).find('.body').slideDown(200);
				$(this).addClass("active");
			}else{
				$('.faq-container').removeClass("active");
				$('.faq-container').find('.body').slideUp(200);
			}
		});
	});
</script>

<script>
	$(function(){
		$("#inquiry").validationEngine({promptPosition : "topLeft"});
		$("#send").click(function() {
			if(window.confirm('お問い合わせを送信します。よろしいですか？')){
				$("#inquiry").submit();
			}
		});
	});
</script>

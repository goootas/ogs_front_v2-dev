<?php
/**
 * メールマガジン登録・解除
 */
class Controller_Mailmagazine extends Controller_Basefront
{
    public function action_index()
    {
        $this->template->title = "メールマガジン";
        $this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/mailmagazine/index');
    }

    /*
     * メルマガ登録
     */
    public function action_regist()
    {
        if (Input::method() == 'POST')
        {
            if ( ! $mailmagazine = Model_Db_Mailmagazine::findEmail(Input::post("email")))
            {
                // 登録
                $mailmagazine = Model_Db_Mailmagazine::forge(array(
                    'email'		=> Input::post('email'),
                    'status'	=> Config::get('status_value.enable'),
                ));
            }
            else
            {
                // 更新
                //$mailmagazine->status = Config::get('status_value.deleted');
                $mailmagazine->status = Config::get('status_value.enable');
            }

            if ($mailmagazine->save())
            {
                Response::redirect('/mailmagazine/complete/regist'.$this->session_get_param);
            }

        }
        $this->template->title = "メールマガジン登録";
        $this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/mailmagazine/regist');
    }

    /*
     * メルマガ解除
     */
    public function action_remove()
    {
        if (Input::method() == 'POST')
        {
            if ( $mailmagazine = Model_Db_Mailmagazine::findEmail(Input::post("email")))
            {
                // 更新
                $mailmagazine->status = Config::get('status_value.deleted');
            }

            if ($mailmagazine->save())
            {
                Response::redirect('/mailmagazine/complete/remove'.$this->session_get_param);
            }

        }
        $this->template->title = "メールマガジン解除";
        $this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/mailmagazine/remove');
    }

    /*
     * メルマガ登録・解除完了
     */
    public function action_complete($type = null)
    {
        $this->template->title = "メールマガジン完了";
        $this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/mailmagazine/complete',array("type" => $type));
    }
}

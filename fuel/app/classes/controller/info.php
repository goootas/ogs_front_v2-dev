<?php
/*
 * インフォメーション
 */
class Controller_info extends Controller_Basefront
{
	/*
	 * info/
	 */
	public function action_index()
	{
		throw new HttpNotFoundException();
	}

	/*
	 * プライバシーポリシー
	 */
	public function action_privacy()
	{
		$data["text"] = $this->shop_desc_data->pcsp_privacy;
		if(!$data["text"]){
			$data["text"] = $this->shop_data["privacy"];
		}

		$this->template->title = "プライバシーポリシー";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/info/index', $data, false);
	}

	/*
	 * 特定商取引法に基づく表示
	 */
	public function action_legal()
	{
		$data["text"] = $this->shop_desc_data->pcsp_legal;
		if(!$data["text"]){
			$data["text"] = $this->shop_data["legal"];
		}

		$this->template->title = "特定商取引法に基づく表示";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/info/index', $data, false);
	}

	/*
	 * ガイド
	 */
	public function action_guide()
	{
		$data["text"] = $this->shop_desc_data->pcsp_guide;
		if(!$data["text"]){
			$data["text"] = $this->shop_data["guide"];
		}

		$this->template->title = "ガイド";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/info/index', $data, false);
	}

	/*
	 * FAQ
	 */
	public function action_faq()
	{
		$data["text"] = $this->shop_desc_data->pcsp_faq;
		if(!$data["text"]){
			$data["text"] = $this->shop_data["faq"];
		}
		$this->template->title = "FAQ";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/info/index', $data, false);
	}

	/*
	 * 退会完了
	 */
	public function action_leave()
	{
		$this->template->title = "退会完了";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/leave/complete');
	}

	/*
	 * 送料表示
	 */
	public function action_postage()
	{
		// 通常送料一覧取得
		$wheres = array();
		$wheres[] = array("shop_id", $this->shop_data["id"]);
		$order = array();
		$order[] = array("prefectures_id","desc");
		$postage = Model_Db_Shop_Postage::findListFront($wheres, $order);
		$this->template->set_global('postage', $postage, false);

		// 離島送料一覧取得
		$rito = Model_Db_Shop_Postage_Rito::find('all', array(
			'where' => array(array(
				"shop_id",$this->shop_data["id"],
			))));
		$this->template->set_global('rito', $rito, false);

		$this->template->title = "送料一覧";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/info/postage');
	}
}

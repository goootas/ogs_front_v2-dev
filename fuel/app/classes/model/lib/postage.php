<?php
namespace Model\lib;

use Model_Db_Shop_Postage_Rito;
use Model_Db_Shop_Postage;
use Model_Db_Prefectures;

class Postage extends \Model
{
	public static function getPostageNoPrice($zip)
	{
		if(!$postal = \Model_Db_Postal::find($zip)){
			return "";
		}
		$data["state"]		= $postal->state;
		$data["city"]		= $postal->city;
		$data["address"]	= $postal->address;

		return $data;
	}

	public static function getPostageNoApi($shop_id,$zip,$state)
	{
		//送料取得※離島
		$search_wheres = array();
		$search_wheres[] = array("shop_id", $shop_id);
		$search_wheres[] = array("zip", $zip);
		$rito = Model_Db_Shop_Postage_Rito::findPriceDetailTool($search_wheres);
		if($rito){
			$data["price"] = $rito->price;
		}else{
			$search_wheres = array();
			$search_wheres[] = array("name", $state);
			$prefectures = Model_Db_Prefectures::findDetailAPI($search_wheres);

			$search_wheres = array();
			$search_wheres[] = array("shop_id", $shop_id);
			$search_wheres[] = array("prefectures_id", $prefectures->id);
			$postage = Model_Db_Shop_Postage::findPriceDetailTool($search_wheres);

			$data["name"] = $prefectures->name;
			if($postage){
				$data["price"] = $postage->price ? $postage->price : 0;
			}else{
				$data["price"] = 0;
			}
		}

		return $data;
	}
}

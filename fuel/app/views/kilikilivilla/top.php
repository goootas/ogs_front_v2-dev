<?php if ($products): ?>
	<?php echo isset($shop_desc->pcsp_top1) ? $shop_desc->pcsp_top1 : "";?>
	<div class="outer">
		<div class="marsonry">
			<ul>
				<?php foreach ($products as $key => $line): ?>
					<li class="box">
						<?php
						$html = '<div class="inner imgchange">';
						if($line["new"]):
							$html .= '<span class="point-ribbon point-ribbon-l" style="color: #4EA5CF">NEW</span>';
						endif;
						$html .= '<div class=""></div><div class="thumbnail">';

						if(isset($line["imgs"][0])){
							$path_front = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0]."?".time();
							$path_back = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0]."?".time();
						}else{
							$path_front = "";
							$path_back = "";
						}
						$img_tag2 = <<< EOM
<img src="//{$path_front}">
<img src="//{$path_back}">
EOM;

						$html .= $img_tag2;

						$html .= '</div>';
						$html .= '<div class="text">';
						if($agent == "sp"){
							$html .= '<dl style="background: linear-gradient(to bottom, rgba(229,0,17,0) 0%,rgba(0, 0, 0, 0.59) 100%);">';
						}else{
							$html .= '<dl>';
						}
						$html .= '<dt>'.$line["title"].'</dt>';
						$html .= '<dd>¥'.number_format(floor($line["price_sale"] * $tax)).'</dd>';
						$html .= '</dl>';
						$html .= '</div>';
						$html .= '</div>';
						?>
						<?php echo Html::anchor('/product/detail/'.$line["id"], $html) ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<?php if(Pagination::instance('list')->current_page != Pagination::instance('list')->total_pages): ?>
		<div id="NEXT" class="buttons">
			<?php echo Pagination::instance('list')->next(); ?>
			<?php echo Asset::img($shop_data["dir"].'/loader.gif', array('id' => 'loading','alt' => '読み込み中','width' => '29','height' => '29',));?>
		</div>
	<?php endif; ?>
<?php else: ?>
	<p>NO ITEM</p>
<?php endif; ?>

<script type="text/javascript">
	$(function($){
		$('.HEADER').removeClass('MIN');
	});
	$(document).on({
		"mouseenter": function(){
			$(this).find("img").attr("src",$(this).find("#out").val());
		},
		"mouseleave": function(){
			$(this).find("img").attr("src",$(this).find("#hover").val());
		}
	}, ".imgchange");

</script>
<?php
$ua = $_SERVER['HTTP_USER_AGENT'];
if(!preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
	?>
	<script type="text/javascript">
		$(function($){
			//タイルレイアウト実行
			function marsonryDo(){
				var windowW = $(window).width()
				var parentW = $(".CONTENTS .marsonry").width()
				var textH = 0;
				if(windowW < 770){
					//こちらでSPの商品数数を設定します。 var boxW = (parentW) / X(数)
					var boxW = (parentW) / 2
					var Hnum = 1
//					$('.box:nth-child(5n+1)').addClass("pickup");
				} else {
					var rand = Math.floor(Math.random()*2 + 1) ;
					//こちらでPCの商品数数を設定します。 var boxW = (parentW) / X(数)
					var boxW = (parentW) / 3
//					$('.box:nth-child(' + rand + ')').addClass("pickup");
					var Hnum = 1
				}
				$('.box .text').each(function(i){
					if(textH <= $(this).height()){
						textH = $(this).height()
					}
//					console.log($(this).innerHeight())
				})
//				console.log(textH)
				$('.box').each(function(i){

					if($(this).hasClass("pickup")){
						$(this).css({
							"width" : boxW * 2+ "px",
							"height" : boxW * 2 + "px"
						});
					} else {
						$(this).css({
							"width" : boxW + "px",
							"height" : boxW + 55 + "px"
						});
					}
					var thumbNailSize = $(this).width()
					$(this).find(".inner").css({
						"width" : (thumbNailSize - 10) + "px",
						"height" : (thumbNailSize + 55 - 10) + "px"
					});
				});
				//clossFadeDo()
				setTimeout(function(){
					$('.marsonry ul').masonry({
						itemSelector: '.box',
						columnWidth: boxW,
						isFitWidth: false
					});
					$('.box').css({
						opacity: 1,
					});
				},100);
			}
			//クロスフェード
			function clossFadeDo(){
				setTimeout(function(){
					$('.box:odd .thumbnail').innerfade({
						speed: 500,
						timeout: 9000,
						type: 'random_start'
					});
				},6000);
				$('.box:nth-child(1) .thumbnail , .box:even .thumbnail').innerfade({
					speed: 500,
					timeout: 9000,
					type: 'random_start'
				});
			}

			function autoLoad(){
				var maxpage = <?php echo $total_pages ?>;
				$('#loading').css('display', 'none');
				$.autopager({
					content: '.marsonry ul',
					link: '#NEXT a',
					autoLoad: true,

					start: function(current, next){
						$('#loading').css('display', 'block');
						$('#NEXT a').css('display', 'none');
					},
					load: function(current, next){
						$('#loading').css('display', 'none');
						$('#NEXT a').css('display', 'block');
						if( current.page >= maxpage ){
							marsonryDo()//パネル実行
							$('#NEXT a').hide();
							return false;
						}else{
							marsonryDo()//パネル実行
						}
					}
				});
				$('#NEXT a').click(function(){
					$.autopager('load');
					return false;
				});
			}
			//読み込み直後実行
			$(window).load(function () {
				setTimeout(function() {
					marsonryDo()
				}, 1000);
				autoLoad()
				$('.flexslider').flexslider({
					animation: "fade"
				});
			});
			//リサイズ時実行
			var timer = false;
			$(window).resize(function() {
				if (timer !== false) {
					clearTimeout(timer);
				}
				timer = setTimeout(function() {
					marsonryDo()
				}, 1000);
			});});
	</script>
<?php } ?>

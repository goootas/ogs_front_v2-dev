<?php
namespace Model\lib;

use Fuel\Core\Config;
use Fuel\Core\DB;
use Fuel\Core\Image;
use Fuel\Core\Log;
use Fuel\Core\Arr;

class Cart extends \Model
{
	/*
	 * カート情報取得
	 */
	public static function getCartDataTax($array_cart)
	{
		Config::load("common");
		// ---------------------------------------------------
		//消費税 TODO:税込み表記に変更するため、一次的に固定
		// ---------------------------------------------------
		$tax = 1.08;

		$cart_data = array();
		$wheres_base = array(
			array("base.status",Config::get('status_value.enable')),
			array("status",Config::get('status_value.enable')),
		);
		foreach ($array_cart as $key => $val) {
			$wheres = array_merge_recursive($wheres_base,array(array("id",$val["did"])));
			$item = \Model_Db_Product_Detail::findProductDetailOne($wheres);
			if($item){
				$tmp = array();
				$tmp['id']	    = $item->base->id;
				$tmp['title']	= $item->base->title;
				$tmp['op1']		= $item->option1_name;
				$tmp['op2']		= $item->option2_name;
				$tmp['price']	= $item->base->price_sale * $tax;

				$tmp['imgs_pc']	= \Model\lib\Image::getImage($item->base->id);
				$tmp['imgs_fp']	= \Model\lib\Image::getImageFp($item->base->id);

				$tmp = array_merge_recursive($val,$tmp);
				$cart_data[] = $tmp;
			}
		}
		return $cart_data;
	}
	/*
	 * カート情報取得
	 */
	public static function getCartData($array_cart)
	{
		Config::load("common");
		// ---------------------------------------------------
		//消費税 TODO:税込み表記に変更するため、一次的に固定
		// ---------------------------------------------------
		$cart_data = array();
//		$wheres_base = array(
//			array("base.status",Config::get('status_value.enable')),
//			array("status",Config::get('status_value.enable')),
//		);
		foreach ($array_cart as $key => $val) {
//			$wheres = array_merge_recursive($wheres_base,array(array("id",$val["did"])));
//			$item = \Model_Db_Product_Detail::findProductDetailOne($wheres);
			$item = \Model_Db_Product_Detail::findDetail($val["did"]);
			if($item){
				$tmp = array();
//				$tmp['id']	    = $item->base->id;
//				$tmp['title']	= $item->base->title;
//				$tmp['op1']		= $item->option1_name;
//				$tmp['op2']		= $item->option2_name;
//				$tmp['price']	= $item->base->price_sale;
//				$tmp['imgs_pc']	= \Model\lib\Image::getImage($item->base->id);
//				$tmp['imgs_fp']	= \Model\lib\Image::getImageFp($item->base->id);
				$tmp['id']	    = $item["id"];
				$tmp['title']	= $item["title"];
				$tmp['op1']		= $item["option1_name"];
				$tmp['op2']		= $item["option2_name"];
				$tmp['price']	= $item["price_sale"];

				$tmp['imgs_pc']	= \Model\lib\Image::getImage($item["id"]);
				$tmp['imgs_fp']	= \Model\lib\Image::getImageFp($item["id"]);

				$tmp = array_merge_recursive($val,$tmp);
				$cart_data[] = $tmp;
			}
		}
		return $cart_data;
	}

	/*
	 * カート不良データ整合
	 */
	public static function adjCartData(&$cart_data,&$error_cnt)
	{
		foreach ($cart_data as $key => $val) {
			if(!$val["did"]){
				Arr::delete($cart_data,$key);
				$error_cnt++;
			}
			if($val["num"] <= 0){
				Arr::delete($cart_data,$key);
				$error_cnt++;
			}
		}
		return;
	}

	/*
	 * 時間在庫チェック
	 *
	 */
	public static function chkStockTimeTable($cart_data,&$error_cnt)
	{

		// TODO 実装中

		return;
	}

	/*
	 * 現地受渡用カートチェック（現地受渡と配送商品混在させない）
	 * false : NG混在している
	 * true  : OK混在していない
	 */
	public static function chkProductReceives($cart_data)
	{
		$error_cnt = 0;
		$tmp_receives_flg = "";
		foreach ($cart_data as $key => $val) {
			$product_detail = \Model_Db_Product_Detail::find($val["did"]);
			if($product_detail){
				if($tmp_receives_flg === ""){
					$tmp_receives_flg = $product_detail->receives_flg;
				}
				if($tmp_receives_flg != $product_detail->receives_flg){
					$error_cnt++;
				}
			}
		}
		if($error_cnt > 0 ){
			return false;
		}else{
			return true;
		}
	}

}
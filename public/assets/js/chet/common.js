$(function(){
    if(!!navigator.userAgent.match(/crios/i)){
        $('.CONSEPT .content-body').addClass('active');
        $('.LINEUP .content-body .innner').addClass('active');
    }

    function clickAction(){
        $(".menubutton").toggle(
            function () {
                $(this).toggleClass("on");
                $(this).siblings(".GLOBALNAVI").fadeIn();
                $(this).siblings(".GLOBALNAVI").toggleClass("on");
                $("body").css("overflow","hidden");
            },
            function () {
                $(this).toggleClass("on");
                $(this).siblings(".GLOBALNAVI").fadeOut();
                $(this).siblings(".GLOBALNAVI").toggleClass("on");
                $("body").css("overflow","auto");
            }
        );
    }

    function floatingHeader(){
        var header = $('.HEADER');
        $(window).scroll(function () {
            if($(window).scrollTop() > 300) {
                if(!header.hasClass("on")){
                    header.clone(true).addClass("clone").insertAfter("body");
                    setTimeout(function(){
                        $('.HEADER.clone').addClass('show')
                    },200);
                    header.addClass("on");
                }
            }else{
                $('.HEADER.clone').removeClass('show')
                setTimeout(function(){
                    $('.HEADER.clone').remove();
                    header.removeClass("on");
                },200);
            }
        });
    }

    function layout(){
        var winW = $(window).width();
        if(winW < 1024){
            $('.HEADLINE .CONTAINER').css({
                height: winW*1.5 + 'px'
            });
        }
    }

    function mainImege(){
        var winW = $(window).width();
        if(winW < 1024){
            $('.titlelogo').attr('src', '/assets/img/chet/IMG_logo_white.png');
            $('.photo-01').attr('src', '/assets/img/chet/IMG_main_02_sp.jpg');
            $('.photo-02').attr('src', '/assets/img/chet/IMG_main_02_sp.jpg');
            $('.photo-03').attr('src', '/assets/img/chet/IMG_main_02_sp.jpg');
            $('.photo-04').remove();
            // $('.photo-01').attr('src', '/assets/img/chet/IMG_main_01_sp.jpg');
            // $('.photo-02').attr('src', '/assets/img/chet/IMG_main_02_sp.jpg');
            // $('.photo-03').attr('src', '/assets/img/chet/IMG_main_03_sp.jpg');
            // $('.photo-04').remove();
        }else{
            $('.titlelogo').attr('src', '/assets/img/chet/IMG_logo.png');
            $('.photo-01').attr('src', '/assets/img/chet/IMG_main_02.jpg');
            $('.photo-02').attr('src', '/assets/img/chet/IMG_main_04.jpg');
            $('.photo-03').attr('src', '/assets/img/chet/IMG_main_02.jpg');
            $('.photo-04').attr('src', '/assets/img/chet/IMG_main_04.jpg');
            // $('.photo-01').attr('src', '/assets/img/chet/IMG_main_01.jpg');
            // $('.photo-02').attr('src', '/assets/img/chet/IMG_main_02.jpg');
            // $('.photo-03').attr('src', '/assets/img/chet/IMG_main_03.jpg');
            // $('.photo-04').attr('src', '/assets/img/chet/IMG_main_04.jpg');
        }
    }

    function rotateTopics(){
        var winW = $(window).width();
        var winH = $(window).height();
        var mainvisualH = $(".mainvisual").height();
        var topics = $('.TOPICS');
        if(winW < 1024){
            if(!$('.TOPICS.clone').length){
                topics.clone(true).addClass("clone flexslider").insertAfter(".mainslider");
                $(".TOPICS.clone").click(function(event) {
                    $(this).toggleClass("on");
                    $(this).next().slideToggle("fast");
                });
                $('.flexslider').flexslider({
                    animation: "slide",
                    animationSpeed: 300,
                    directionNav: false,
                    controlNav: false
                });
                $('.TOPICS.clone .btn').remove();
                $('.HEADLINE .CONTAINER').css({
                    height: 'auto'
                });

            }
        }else{
            $('.TOPICS.clone').remove();
        }
        $('.TOPICS.clone').css({
            top: mainvisualH - 65
        });
    }

    function plugIn(){


        var winW = $(window).width();
        var winH =  $(window).height();
        if(winW < 1024){
            var triggerPoint = winH / 3.3;
            $('.TOP .LINEUP  .content-head').waypoint(function(){
                $('.LINEUP .content-body .innner').each(function(index) {
                    var $this = $(this)
                    setTimeout(function(){
                        $this.addClass('active');
                    },150*index);
                });

            },{
                offset : triggerPoint + 'px'
            });
            $('.CONSEPT').waypoint(function(){
                $('.CONSEPT .content-body').addClass('active');
            },{
                offset : triggerPoint + 'px'
            });
        }else{
            $('.TOP .LINEUP').waypoint(function(){
                $('.LINEUP .content-body .innner').each(function(index) {
                    var $this = $(this)
                    setTimeout(function(){
                        $this.addClass('active');
                    },150*index);
                });

            },{
                offset : '0%'
            });
            $('.TOP .CONSEPT').waypoint(function(){
                $('.CONSEPT .content-body').addClass('active');
            },{
                offset : '0%'
            });
        }


    }

    function mainRotation(){
        setInterval(function(){
            var H = $('.mainslider > img').height();
            $('.mainslider').css({
                height: H + 'px'
            });
        },100);

        $('.mainslider img:gt(0)').hide();
        setInterval(function(){
                $('.mainslider :first-child').fadeOut(800)
                    .next('img').fadeIn(800)
                    .end().appendTo('.mainslider');},
            5000);
    }

    $(window).load(function() {
        layout();
        floatingHeader();
        mainImege();
        mainRotation();
        clickAction();
        setTimeout(function(){
            plugIn();
            rotateTopics();
        },700);

    });

    $(window).resize(function() {
        mainImege();
        rotateTopics();
    });

});





<?php
use Model\lib\Postage;

class Controller_Api_Postage extends Controller_Baseapi
{
	public function get_address()
	{
		$data = Postage::getPostageNoPrice(Input::get("zip"));
		return $this->response( $data );
	}
}

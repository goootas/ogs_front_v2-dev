<style>
	.CONTENTS {
		margin-top: 0;
	}
	.CONTENTS .CONTAINER {
		max-width: 100%;
		padding: 0;
		width: 100%;
		border-radius: 0;
	}
	.outer{
		height: 100%;
		margin: 0 auto;
		max-width: 960px;
		overflow: hidden;
		padding: 0 10px;
	}
	.headline{
		margin: 80px 15px 40px;
	}
	.headline::before {
		background-color: #fff;
	}
	.headline::after {
		background-color: #fff;
	}
	@media screen and (max-width: 1024px) {
		.CONTENTS .CONTAINER {
			padding: 0 !important;
			width: auto;
		}
		.outer{
			padding:0px;
		}
	}
</style>
<?php if ($products): ?>
	<?php if(Request::active()->controller == 'Controller_Top') :?>
		<div class="MAINSLIDER">
			<img src="<?php echo Config::get("assets_url");?>img/magic-kyun/IMG_headline_info01.jpg" class="">
		</div>

	<?php endif; ?>

	<div class="FREE-TEXT HEAD BUNNER">
		<?php if($banner): ?>
			<?php foreach($banner as $b): ?>
				<?php
				$img_path = "https:".Config::get("banner_url").$b->id.'_pc.jpg';
				$array = get_headers($img_path);
				if(strpos($array[0],'OK')){
					?>

					<?php if($b->url == ""){?>
						<?php echo Asset::img($img_path, array('alt' => $b->title,'style' => "margin-bottom: 2px;"));?>
					<?php }else{?>
						<a href="<?php echo $b->url;?>" target="_blank"><?php echo Asset::img($img_path, array('alt' => $b->title));?></a>
					<?php }?>

				<?php } ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>

	<div class="UP">
		<?php echo isset($shop_desc->pcsp_top1) ? $shop_desc->pcsp_top1 : "";?>
	</div>

	<div class="outer">
		<h4 class="headline">ITEM</h4>
		<div class="marsonry">
			<ul>
				<?php foreach ($products as $key => $line): ?>
					<li class="box">
						<?php
						$html = '<div class="inner imgchange">';
						$html .= '<div class=""></div><div class="thumbnail">';
						if(isset($line["imgs"][0])){
//							$path_front = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0]."?".time();
//							$path_back = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0]."?".time();
							$path_front = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0];
							$path_back = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0];
						}else{
							$path_front = "";
							$path_back = "";
						}
						$img_tag2 = <<< EOM
<img src="//{$path_front}">
EOM;
						$html .= $img_tag2;
						$html .= '</div>';
						$html .= '<figcaption>';
						$html .= '<strong>';
						$html .=$line["body_list"];
						$html .= '</strong>';
						$html .= '</figcaption>';
						$html .= '</div>';/*inner閉じ*/
						?>
						<?php echo Html::anchor('/product/detail/'.$line["id"], $html) ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
    
    <div class="FREE-TEXT FOOT">
        <div class="frame-head">
       	 <img src="/assets/img/magic-kyun/footer_frame_top-paper.png">
        </div>
		<div class="frame-body">
            <h3>INFORMATION</h3>
            <h4>第1回受付<!---：ご自宅等への商品配送---></h4>
            <p>受付：◯/◯(◯)00:00〜◯/◯(◯)00:00<br>
            配送：◯月◯旬以降順次発送予定</p>
            
            <!---
            <h4>第2回受付：ライブ会場での商品受取</h4>
            <p>受付：1/11(金)12:00〜1/24(火)23:59<br />
            受取：1/28(土),29(日)横浜アリーナ会場内<br />
            <font color="#BA4C88"><a href="/macross/page/index/10" style="text-decoration: underline">注文/商品受取方法</a></font></p>
            --->
            
            <h3></h3>
            
            <small>
            <!---■お1人様各商品1点まで、ご注文はお1人様1回のみとさせていただきます。<br>--->
            ■配送受取でのご注文の場合、商品代金に加えて、全国一律(北海道・沖縄含む)配送料¥572(税込)を頂きます。(お届けは日本国内のみ) <!--なお、1回のご注文金額(税込/商品代金合計)¥◯,000以上で送料が無料となります。--><br>
            ■決済方法により、別途、決済手数料が発生する場合がございます。ご購入の際にご確認ください。<br>
            ■受付開始直後のアクセス集中のためサイトにつながりにくい場合は、しばらく時間をおいて再度アクセスいただけますようお願いします。<br>
            ■注文確定後の変更/キャンセルは承ることができません。ご注文の際は十分にご検討の上お申し込みください。<br>
            ■同受付期間内の注文同士であっても、複数の注文を1つの注文にまとめることはできません。ご注文の際は十分にご検討のうえお申し込みください。<br>
            ■別々にいただいた配送受取でのご注文は、一つにまとめて出荷(同梱出荷)することはできません。<br>
            ■商品在庫数には限りがあります。お申し込み多数の際は品切れとなる場合がございます。<br>
            ■注文確認メールが届かない場合は、<a href="/magic-kyun/mypage" style="text-decoration: underline">マイページ</a>注文履歴をご確認ください。(お客様情報登録された方のみ / 未登録の方は<a href="/magic-kyun/inquiry" style="text-decoration: underline">お問い合わせページ</a>よりお問い合わせください)<br>
            ■画像と実際の商品は色合い等、若干異なる場合がございますので、ご了承ください。<br>
            ■各商品表示スペック・サイズ等をお確かめの上、お申し込みください。<br>
            ■衣料商品は、特製上表示サイズと若干の誤差が生じる場合がございますので、あらかじめご了承ください。<br>
            ■配送受取でのご注文の場合、悪天候や交通事情等でお届けが遅れる場合がございます。<br>
            ■営利目的、転売目的でのご注文はお断りいたします。<br>
            ■通販会社(株)RENIにて販売代行しております。					
            </small>
        </div>
        <div class="frame-foot">
       	 <img src="/assets/img/magic-kyun/footer_frame_bottom-paper.png">
        </div>
    </div>
    
	<?php if(Pagination::instance('list')->current_page != Pagination::instance('list')->total_pages): ?>
		<div id="NEXT" class="buttons">
			<?php echo Pagination::instance('list')->next(); ?>
			<?php echo Asset::img($shop_data["dir"].'/loader.gif', array('id' => 'loading','alt' => '読み込み中','width' => '29','height' => '29',));?>
		</div>
	<?php endif; ?>
<?php else: ?>
	<p>NO ITEM</p>
<?php endif; ?>

<script type="text/javascript">
	$(function($){
		$('.HEADER').removeClass('MIN');
	});
	$(document).on({
		"mouseenter": function(){
			$(this).find("img").attr("src",$(this).find("#out").val());
		},
		"mouseleave": function(){
			$(this).find("img").attr("src",$(this).find("#hover").val());
		}
	}, ".imgchange");

</script>
<?php
$ua = $_SERVER['HTTP_USER_AGENT'];
if(!preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
	?>
	<script type="text/javascript">
		$(function($){
			//タイルレイアウト実行
			function marsonryDo(){
				var windowW = $(window).width()
				var parentW = $(".CONTENTS .marsonry").width()
				if(windowW < 728){
					var boxW = (parentW) / 2
					var Hnum = 1.44
				} else {
					var rand = Math.floor(Math.random()*2 + 1) ;
					var boxW = (parentW) / 3
					var Hnum = 1.45
				}

				$('.box').each(function(i){
					if($(this).hasClass("pickup")){
						$(this).css({
							"width" : boxW * 2+ "px",
							"height" : boxW * 2 + "px"
						});
					} else {
						$(this).css({
							"width" : boxW + "px",
							"height" : boxW * Hnum + "px"
						});
					}
					var thumbNailSize = $(this).width()
					$(this).find(".inner").css({
						"width" : (thumbNailSize - 10) + "px",
						"height" : (thumbNailSize * Hnum - 10) + "px"
					});
				});
				clossFadeDo()
				setTimeout(function(){
					$('.marsonry ul').masonry({
						itemSelector: '.box',
						columnWidth: boxW,
						isFitWidth: false
					});
					$('.box').css({
						opacity: 1,
					});
				},100);
			}
			//クロスフェード
			function clossFadeDo(){
				setTimeout(function(){
					$('.box:odd .thumbnail').innerfade({
						speed: 500,
						timeout: 9000,
						type: 'random_start'
					});
				},6000);
				$('.box:nth-child(1) .thumbnail , .box:even .thumbnail').innerfade({
					speed: 500,
					timeout: 9000,
					type: 'random_start'
				});
			}

			function autoLoad(){
				var maxpage = <?php echo $total_pages ?>;
				$('#loading').css('display', 'none');
				$.autopager({
					content: '.marsonry ul',
					link: '#NEXT a',
					autoLoad: true,

					start: function(current, next){
						$('#loading').css('display', 'block');
						$('#NEXT a').css('display', 'none');
					},
					load: function(current, next){
						$('#loading').css('display', 'none');
						$('#NEXT a').css('display', 'block');
						if( current.page >= maxpage ){
							marsonryDo()//パネル実行
							$('#NEXT a').hide();
							return false;
						}else{
							marsonryDo()//パネル実行
						}
					}
				});
				$('#NEXT a').click(function(){
					$.autopager('load');
					return false;
				});
			}
			//読み込み直後実行
			$(window).load(function () {
				setTimeout(function() {
					marsonryDo()
				}, 1000);
				autoLoad()
				$('.flexslider').flexslider({
					animation: "slide",
					directionNav: true,
					prevText: "",
					nextText: ""
				});
			});
			//リサイズ時実行
			var timer = false;
			$(window).resize(function() {
				if (timer !== false) {
					clearTimeout(timer);
				}
				timer = setTimeout(function() {
					marsonryDo()
				}, 1000);
			});});
	</script>
<?php } ?>


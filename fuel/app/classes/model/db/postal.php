<?php
class Model_Db_Postal extends \Orm\Model
{
	protected static $_properties = array(
		'zip',
		'state',
		'city',
		'address'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_postal';
	protected static $_primary_key = array('zip');

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		return $val;
	}
}

<?php
class Model_Db_User extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'shop_id',
        'email',
        'password',
        'username_sei',
        'username_mei',
        'username_sei_kana',
        'username_mei_kana',
        'zip',
        'prefecture',
        'address1',
        'address2',
        'tel1',
        'tel2',
        'tel3',
        'sex',
        'birthday',
        'mailmagazine',
        'funclub_flg',
        'status',
        'last_login',
        'insert_date',
        'update_date',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => true,
            'property' => 'insert_date',
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => true,
            'property' => 'update_date',
        ),
    );
    protected static $_table_name = 'tbl_user';

    protected static $_has_one = array(
        'shop' => array(
            'key_from' => 'shop_id',
            'model_to' => 'Model_Db_Shop',
            'key_to' => 'id',
            'cascade_save' => false,
            'cascade_delete' => false,
            ),
    );

    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        switch ($factory) {
            case 'regist':
                $val->add('username_sei', 'お名前')->add_rule('required');
                $val->add('username_mei', 'お名前')->add_rule('required');
                $val->add('password', 'パスワード')->add_rule('required');
                break;
            case 'regist2':
                $val->add('username_sei', 'お名前')->add_rule('required');
                $val->add('username_mei', 'お名前')->add_rule('required');
                $val->add('username_sei_kana', 'ふりがな')->add_rule('required');
                $val->add('username_mei_kana', 'ふりがな')->add_rule('required');
                $val->add('password', 'パスワード')->add_rule('required');
                $val->add('email', 'メールアドレス')->add_rule('required');
                $val->add('state', '都道府県')->add_rule('required');
                $val->add('address1', '住所１')->add_rule('required');
                $val->add('address2', '住所２')->add_rule('required');
                $val->add('tel1', '電話番号')->add_rule('required')->add_rule('valid_string', array('numeric'));
                $val->add('tel2', '電話番号')->add_rule('required')->add_rule('valid_string', array('numeric'));
                $val->add('tel3', '電話番号')->add_rule('required')->add_rule('valid_string', array('numeric'));
                break;
            case 'login':
                $val->add('email', 'メールアドレス')->add_rule('required');
                $val->add('password', 'パスワード')->add_rule('required');
                break;
            case 'forget_email':
                $val->add('email', 'メールアドレス')->add_rule('required');
                break;
            case 'forget_pass':
                $val->add('password', 'パスワード')->add_rule('required');
                break;
            default:
                break;
        }
        return $val;
    }

    public static function findEmail($email)
    {
        $data = static::find('first', array(
                'where' => array(
                    array("status", Config::get("status_value.enable")),
                    array("email", $email),
                ))
        );
        return $data;
    }

    public static function findEmail2($shop_id,$email)
    {
        $data = static::find('first', array(
                'where' => array(
                    array("status", Config::get("status_value.enable")),
                    array("shop_id", $shop_id),
                    array("email", $email),
                ))
        );
        return $data;
    }
    public static function findEmail3($shop_id,$user_id,$email)
    {
        $data = static::find('first', array(
                'where' => array(
                    array("status", Config::get("status_value.enable")),
                    array("shop_id", $shop_id),
                    array("id","!=", $user_id),
                    array("email", $email),
                ))
        );
        return $data;
    }
    public static function findEmailPassword($shop_id,$email,$pass)
    {
        $data = static::find('first', array(
            'where' => array(
                array("status", Config::get("status_value.enable")),
                array("shop_id", $shop_id),
                array("email", $email),
                array("password", $pass),
            ))
        );
        return $data;
    }

    public static function findListTool($where="",$sort="")
    {
        $data = static::find('all', array(
            'related'	=> array('shop'),
            'where'		=> $where,
            'order_by'	=> $sort,
            )
        );
        return $data;
    }
    /*
     * 最終ログイン
     *
     * @return void
     */
    public static function setLastLogin($id) {

        $data = self::find($id);
        $data->last_login = date('Y-m-d H:i:s');
        $data->save();

        return;
    }
}

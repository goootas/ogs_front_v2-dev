<?php
class Model_Db_Cms_Page extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'title',
		'title_list',
		'view_date_from',
		'view_date_to',
		'redirect_url',
		'sort',
		'memo',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_cms_page';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		return $val;
	}

	public static function findList($where="",$sort="",$paginations="")
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= " FROM " . self::$_table_name;
		$sql .= " WHERE 1 = 1 ";
		$sql .= " AND status >= 0 ";
		$sql .= " AND shop_id IN (".$where["shop_ids"].") ";
		if(isset($where["shop_id"])){
			$sql .= "AND shop_id =".$where["shop_id"]." ";
		}
		if(isset($where["view_date_from"])){
			$sql .= "AND view_date_from >= '".$where["view_date_from"]."' ";
		}
		if(isset($where["view_date_to"])){
			$sql .= "AND view_date_to <= '".$where["view_date_to"]."' ";
		}
		$sql .= " ORDER BY sort ";

		if($paginations){
			$sql .= " LIMIT ".$paginations->per_page;
			$sql .= " OFFSET ".$paginations->offset;
		}

		$query = DB::query($sql);
		$data = $query->execute()->as_array();

		return $data;
	}

	public static function findDetail($where)
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= " FROM " . self::$_table_name;
		$sql .= " WHERE 1 = 1 ";
		$sql .= " AND id = ".$where["page_id"];
		$sql .= " AND status = 1 ";
		$sql .= " AND shop_id =".$where["shop_id"]." ";
		$sql .= " AND view_date_from <= now() ";
		$sql .= " AND view_date_to >= now() ";

		$query = DB::query($sql);
		$data = $query->execute()->current();

		return $data;
	}
}


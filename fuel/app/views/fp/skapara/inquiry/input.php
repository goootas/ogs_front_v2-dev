<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
<?php echo isset($shop_desc->fp_faq) ? $shop_desc->fp_faq : "";?>

<h4>お問い合わせ</h4>
<?php echo Form::open();?>
<div>
	<div>
		お名前(必須)<br>
		<?php echo Form::input('username', Input::post('username', isset($user_data)  ? $user_data->username_sei . $user_data->username_mei : "")); ?>
	</div>
	<div>
		メールアドレス(必須)<br>
		<?php echo Form::input('email', Input::post('email', isset($user_data)  ? $user_data->email : ""), array("istyle"=>"3", "format"=>"*x", "MODE"=>"alphabet")); ?>
	</div>
	<div>
		お問い合わせ内容(必須)<br>
		<?php echo Form::textarea('message', Input::post('message', ''),array('rows' =>'10')); ?>
	</div>

	<br>
	<font color="red">
		メールによるお問い合わせの回答は原則、祝日 / 年末年始を除く月～金曜 12:00～17:00の間となります。<br><br>
		携帯電話会社等が提供しているメールアドレスは受信に関する規制が多く、メールが届かない場合があります。<br><br>
		ドメイン指定受信設定されている方は「**@official-goods-store.jp」からのメールを受信できるようにしてください。<br>
	</font>
	<br>
<!--	<font color="red">-->
<!--		■年末年始のお問い合わせ窓口について<br>-->
<!--		12/25(金)～1/3(日)の間、お問い合わせ窓口を休業いたします。<br>-->
<!--		※メールでの受付は可能ですが、返信は1/4(月)以降となる可能性があります。<br>-->
<!--	</font>-->
<!--	<br>-->

	<div>
		<?php echo Form::submit('exec', '送信'); ?><br>
	</div>
</div>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
<?php echo Form::close();?>

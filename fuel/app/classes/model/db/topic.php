<?php
class Model_Db_Topic extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'title',
		'body',
		'view_date_from',
		'view_date_to',
		'redirect_url',
		'sort',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_topic';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		return $val;
	}

	public static function findList($where="",$paginations="")
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " *, ";
		$sql .= " DATE_FORMAT(view_date_from,'%Y.%m.%d [%a]') as view_date ";
		$sql .= " FROM " . self::$_table_name;
		$sql .= " WHERE 1 = 1 ";
		$sql .= " AND status = 1 ";
		$sql .= " AND shop_id =".$where["shop_id"]." ";

		if(isset($where["view_date_from"]))
		{
			$sql .= "AND view_date_from <= '".$where["view_date_from"]."' ";
		}else
		{
			$sql .= "AND view_date_from <= now() ";
		}
		if(isset($where["view_date_to"]))
		{
			$sql .= "AND view_date_to >= '".$where["view_date_to"]."' ";
		}else
		{
			$sql .= "AND view_date_to >= now() ";
		}

		$sql .= " ORDER BY view_date_from desc";

		if($paginations){
			$sql .= " LIMIT ".$paginations->per_page;
			$sql .= " OFFSET ".$paginations->offset;
		}

		$query = DB::query($sql);
		$data = $query->execute()->as_array();

		return $data;
	}
	public static function findTopicOne($where="",$sort="",$paginations="")
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " *, ";
		$sql .= " DATE_FORMAT(view_date_from,'%Y.%m.%d [%a]') as view_date ";
		$sql .= " FROM " . self::$_table_name;
		$sql .= " WHERE 1 = 1 ";
		$sql .= " AND status = 1 ";
		$sql .= " AND shop_id =".$where["shop_id"]." ";

		if(isset($where["id"]))
		{
			$sql .= "AND id = ".$where["id"]." ";
		}
		if(isset($where["view_date_from"]))
		{
			$sql .= "AND view_date_from <= '".$where["view_date_from"]."' ";
		}else
		{
			$sql .= "AND view_date_from <= now() ";
		}
		if(isset($where["view_date_to"]))
		{
			$sql .= "AND view_date_to >= '".$where["view_date_to"]."' ";
		}else
		{
			$sql .= "AND view_date_to >= now() ";
		}

		$sql .= " ORDER BY view_date_from desc";

		if($paginations){
			$sql .= " LIMIT ".$paginations->per_page;
			$sql .= " OFFSET ".$paginations->offset;
		}

		$query = DB::query($sql);
		$data = $query->execute()->current();

		return $data;
	}
	public static function findTopicOneAll($where="",$sort="",$paginations="")
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " *, ";
		$sql .= " DATE_FORMAT(view_date_from,'%Y.%m.%d [%a]') as view_date ";
		$sql .= " FROM " . self::$_table_name;
		$sql .= " WHERE 1 = 1 ";
		$sql .= " AND shop_id =".$where["shop_id"]." ";

		if(isset($where["id"]))
		{
			$sql .= "AND id = ".$where["id"]." ";
		}
		if(isset($where["view_date_from"]))
		{
			$sql .= "AND view_date_from <= '".$where["view_date_from"]."' ";
		}else
		{
			$sql .= "AND view_date_from <= now() ";
		}
		if(isset($where["view_date_to"]))
		{
			$sql .= "AND view_date_to >= '".$where["view_date_to"]."' ";
		}else
		{
			$sql .= "AND view_date_to >= now() ";
		}

		$sql .= " ORDER BY view_date_from desc";

		if($paginations){
			$sql .= " LIMIT ".$paginations->per_page;
			$sql .= " OFFSET ".$paginations->offset;
		}

		$query = DB::query($sql);
		$data = $query->execute()->current();

		return $data;
	}
	public static function findTopicList($where="",$limit="")
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " *, ";
		$sql .= " DATE_FORMAT(view_date_from,'%Y.%m.%d [%a]') as view_date ";
		$sql .= " FROM " . self::$_table_name;
		$sql .= " WHERE 1 = 1 ";
		$sql .= " AND status = 1 ";
		$sql .= " AND shop_id =".$where["shop_id"]." ";

		if(isset($where["id"]))
		{
			$sql .= "AND id = ".$where["id"]." ";
		}
		if(isset($where["view_date_from"]))
		{
			$sql .= "AND view_date_from <= '".$where["view_date_from"]."' ";
		}else
		{
			$sql .= "AND view_date_from <= now() ";
		}
		if(isset($where["view_date_to"]))
		{
			$sql .= "AND view_date_to >= '".$where["view_date_to"]."' ";
		}else
		{
			$sql .= "AND view_date_to >= now() ";
		}

		$sql .= " ORDER BY view_date_from desc";


		if($limit)$sql .= " LIMIT ".$limit;

		$query = DB::query($sql);
		$data = $query->execute()->as_array();

		return $data;
	}
}


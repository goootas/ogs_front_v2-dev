<?php // TODO: JQueryを使わないで処理する方法に変更　?>

<h4>パスワード再設定</h4>
<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<p>
	パスワード再設定用URLを送付いたします。<br>
	お客様のメールアドレスを入力して、送信ボタンを押してください。<br>
</p>

<?php echo Form::open();?>
<div>
	<p><b>メールアドレス</b></p>
</div>
<div>
	<?php echo Form::input('email', Input::post('email', ''), array("istyle"=>"3", "format"=>"*x", "MODE"=>"alphabet")); ?>
</div>
<div>
	<?php echo Form::submit('exec', '送信'); ?>
</div>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

<font color="red">通販サイトリニューアルに伴い、2015年6月以前に会員登録されたお客様は、大変お手数ですが、再度新規会員登録をお願いいたします。</font><br>

<?php echo Form::close();?>

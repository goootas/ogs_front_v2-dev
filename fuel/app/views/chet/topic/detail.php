<div class="TOPICS SINGLE">
	<div class="CONTAINER">
		<div class="content-head">
			<div class="content-headline">
				<h1>TOPICS</h1>
				<hr>
			</div>
		</div>
		<div class="content-body">

			<article>
				<?php if ($topics): ?>
					<header>
						<h1><?php echo $topics["title"];?></h1>
						<p>UPDATE: <?php echo $topics["view_date"];?></p>
					</header>
					<section>
						<p><?php echo $topics["body"];?></p>
					</section>
				<?php else: ?>
				<?php endif; ?>



				<footer>
					<h1>ARCHIVE</h1>
					<ul>

						<?php if ($topics_archive): ?>
							<?php foreach ($topics_archive as $key => $line): ?>

								<li>
									<?php $tag = '<dl><dt>'.$line["view_date"].'</dt><dd class="">'.$line["title"].'</dd></dl>'; ?>
									<?php echo Html::anchor('/topic/detail/'.$line["id"], $tag, array("class" => "innner")) ?>
								</li>

							<?php endforeach; ?>
						<?php else: ?>
						<?php endif; ?>

					</ul>
					<?php echo Html::anchor('/topic', "SHOW MORE" ,array('class' => 'btn primary')) ?>
				</footer>

			</article>

		</div>
	</div>
</div>

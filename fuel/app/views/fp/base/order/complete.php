<h4>注文完了</h4>

<div>
	<p>
		ご注文いただき誠にありがとうございました。<br>
		受注番号：<?php echo $order->order_id;?><br><br>
		注文確認のメールを「<?php echo $order->order_email;?>」宛に送信しております。
	</p>
	<br>
	<?php if ($order->payment == 2) : ?>
		<p>
			オンライン決済番号または、受付番号を紙などに控えてご指定いただいたコンビニエンスストアにて、下記の支払い期限までにお支払いください。<br><br>
			お支払い先コンビニ：<?php echo Config::get("cvs.".$order->cvs_type);?><br>
			<?php if (substr($order->cvs_type ,0,-1) == "econ") : ?>
				受付番号：<?php echo $order->cvs_receipt_no;?><br>
			<?php elseif (substr($order->cvs_type ,0,-1) == "sej") : ?>
				オンライン決済番号：<?php echo $order->cvs_receipt_no;?><br>
			<?php elseif (substr($order->cvs_type ,0,-1) == "other") : ?>
				オンライン決済番号：<?php echo $order->cvs_receipt_no;?><br>
			<?php endif;?>
			<?php if ($order->cvs_haraikomi_url) : ?>
				払込票URL：<a href="<?php echo $order->cvs_haraikomi_url;?>" target="_blank"><?php echo $order->cvs_haraikomi_url;?></a><br>
			<?php endif;?>
			支払期限：<?php echo $order->cvs_limit_date;?><br>
		</p>
	<?php endif;?>


	※注文確認メールが届かない場合<br>
	注文確認メールが届かない場合でも、正常にご注文をお受けさせていただいております。<br>
	表示された受注番号にてマイページより内容をご確認ください。<br>
	<br>
	＊注文確認メールが届かない原因<br>
	(!) メールアドレス間違い<br>
	(!) 迷惑メールボックスに注文確認メールが入っている<br>
	(!) スマートフォンやフィーチャーフォンをご利用の場合、「PCからのメールを受信しない」「なりすまし設定を有効にしている」等のメール受信設定に引っかかっている<br>
	<br>
<!--	<p class="small">数分たっても届かない場合には上記をご確認の上、--><?php //echo Html::anchor('/inquiry'.$session_get_param,'お問い合わせ') ?><!--よりご連絡ください。</p>-->
	<?php if ($order->payment == "4") : ?>
	※代金のお支払いについて	<br>
	・当店にかわり、後払い.com運営会社の(株)キャッチボールより請求書が送られます。	<br>
	・商品到着と請求書の到着は別になります。<br>
	・請求書発行から14日後までにお支払い下さい。<br>
	・銀行 / 郵便局 / コンビニでお支払いいただけます。<br>
	<?php endif;?>

</div>


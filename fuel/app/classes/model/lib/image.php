<?php
namespace Model\lib;

use Fuel\Core\Cache;
use Fuel\Core\Config;
use Fuel\Core\Log;
use Fuel\Core\Upload;

use Model_Lib_S3image;

class Image extends \Model
{
	public static function setImage($id)
	{
		// 画像ファイルアップロード
		Upload::process();
		if (Upload::is_valid()) {
			foreach(Upload::get_files() as $key => $file){
				$tmp = self::getImage($id);
				Log::debug(print_r(count($tmp),true));
				Log::debug(print_r($id."_".count($tmp).".".$file["extension"],true));
				if(! Model_Lib_S3image::setFileSave($id, $id."_".sprintf("%03d", count($tmp)).".".$file["extension"], $file["file"])){
					$error[]= "画像ファイルの登録に失敗しました。";
				}
			}
		} else {
			foreach (Upload::get_errors() as $key => $error) {
				if ($error['errors'][0]['error'] != Upload::UPLOAD_ERR_NO_FILE) {
					$error[]= $error['errors'][0]['message'];
				}
			}
		}
		if(isset($error)) {
			Log::warning(__FILE__ . __LINE__ . print_r($error,true));
			return false;
		}
		return true;

	}
	public static function getImage($id)
	{
		try
		{
			$images = Cache::get("product_imgs_sp_".$id);
		}
		catch (\CacheNotFoundException $e)
		{
			Config::load("common");
			$s3base_url = Config::get("s3base_url");
			$obj = \Model_Db_Product_Obj::findImageListUrl($id,"pc");
			$images = array();
			foreach ($obj as $val) {
				$images[] = str_replace($s3base_url, "", $val["obj_url"]);
			}
			Cache::set("product_imgs_sp_".$id, $images, 60 * 5);
		}

//		$images = array();
//		$tmp = Model_Lib_S3image::getList($id);
//		foreach ($tmp as $object) {
//			$images[] = $object["Key"];
//		}
//		Log::debug(print_r($images,true));
		return $images;
	}
	public static function getImageFp($id)
	{
		try
		{
			$images = Cache::get("product_imgs_fp_".$id);
		}
		catch (\CacheNotFoundException $e)
		{
			Config::load("common");
			$s3base_url = Config::get("s3base_url");

			$obj = \Model_Db_Product_Obj::findImageListUrl($id,"fp");
			$images = array();
			foreach ($obj as $val) {
				$images[] = str_replace($s3base_url, "", $val["obj_url"]);
			}
			Cache::set("product_imgs_fp_".$id, $images, 60 * 5);
		}

//		$images = array();
//		$tmp = Model_Lib_S3image::getListFp($id);
//		foreach ($tmp as $object) {
//			$images[] = $object["Key"];
//		}
//		Log::debug(print_r($images,true));
		return $images;
	}
}
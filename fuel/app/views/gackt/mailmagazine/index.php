<?php echo Form::open(array('action' => '/mailmagazine/regist' ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<fieldset>
	<div class="form-group">
		<div class="col-sm-6">
		<label class='control-label'>登録される方はこちらにメールアドレスを入力してください</label>
		<?php echo Form::input('email', '', array('class' => 'form-control', 'placeholder'=>'xxxxx@xxxx.com')); ?>
		<?php echo Form::submit('submit', '登録', array('class' => 'btn btn-primary')); ?>
		</div>
	</div>
</fieldset>
<?php echo Form::close();?>

<?php echo Form::open(array('action' => '/mailmagazine/remove' ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<fieldset>
	<div class="form-group">
		<div class="col-sm-6">
		<label class='control-label'>解除される方はこちらにメールアドレスを入力してください</label>
		<?php echo Form::input('email', '', array('class' => 'form-control', 'placeholder'=>'xxxxx@xxxx.com')); ?>
		<?php echo Form::submit('submit', '解除', array('class' => 'btn btn-primary')); ?>
		</div>
	</div>
</fieldset>
<?php echo Form::close();?>

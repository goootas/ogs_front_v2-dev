<?php
class Model_Db_Cms_Cell extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'page_id',
		'title',
		'device_type',
		'body',
		'view_date_from',
		'view_date_to',
		'sort',
		'memo',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_cms_cell';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		return $val;
	}

	public static function findList($where="")
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= " FROM " . self::$_table_name;
		$sql .= " WHERE 1 = 1 ";
		$sql .= " AND status = 1 ";
		$sql .= " AND view_date_from <= now() ";
		$sql .= " AND view_date_to >= now() ";
		$sql .= " AND page_id =".$where["page_id"]." ";

		if(isset($where["device_type"])){
			$sql .= "AND (device_type = 0 ";
			$sql .= "OR device_type = ".$where["device_type"]." ) ";
		}

		$sql .= " ORDER BY sort ";

		$query = DB::query($sql);
		$data = $query->execute()->as_array();

		return $data;
	}
}


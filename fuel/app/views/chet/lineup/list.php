<div class="LINEUP">
	<div class="CONTAINER">
		<div class="content-head">
			<div class="content-headline">
				<h1>LINEUP</h1>
				<hr>
			</div>
		</div>
		<div class="content-body">
			<?php if ($products): ?>
				<?php foreach ($products as $key => $line): ?>
					<?php
					$tag = '<div class="innner">';
					$tag .= '<figure>';
					$tag .= '<img src="//'.$shop_data["s3bucket"].'.s3-ap-northeast-1.amazonaws.com/'.$line["imgs"][0]."?".time().'" alt="'.$line["title"].'">';
					$tag .= $line["body_list"];
					$tag .= '</figure>';
					$tag .= '<span class="btn primary">SHOW MORE</span>';
					$tag .= '</div>';
					?>
					<?php //echo $tag; ?>
					<?php echo Html::anchor('/product/detail/'.$line["id"], $tag) ?>
				<?php endforeach; ?>
			<?php else: ?>
			<?php endif; ?>
		</div>
	</div>
</div>
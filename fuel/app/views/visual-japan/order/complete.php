<div class="panel complete">
	<h4 class="subheadline">注文完了</h4>
	<h5 class="heading">ご注文ありがとうございました。<br><span>受注番号：<?php echo $order->order_id;?></span></h5>

	<div class="description">
		<p>
			会場で商品をお受け取りになる際、上記受注番号下5桁の提示が必要となります。<br />
			メモをお取りになるなど、大切に保管してください。<br />
			<br />
			なお、上記受注番号は「<?php echo $order->order_email;?>」宛に送信させていただいた注文確認メール内や、マイページ注文履歴内にも記載されおてります。（マイページは、お客様情報登録された方のみ閲覧可能です）<br />

			<?php if ($order->payment == 2) : ?>
				<br />
				■支払方法<br />
				オンライン決済番号または受付番号を控えて、ご指定いただいたコンビニエンスストアにて下記の支払い期限までにお支払いください。<br><br>
				支払い先コンビニ：<?php echo Config::get("cvs.".$order->cvs_type);?><br>
				<?php if (substr($order->cvs_type ,0,-1) == "econ") : ?>
					受付番号：<?php echo $order->cvs_receipt_no;?><br>
				<?php elseif (substr($order->cvs_type ,0,-1) == "sej") : ?>
					オンライン決済番号：<?php echo $order->cvs_receipt_no;?><br>
				<?php elseif (substr($order->cvs_type ,0,-1) == "other") : ?>
					オンライン決済番号：<?php echo $order->cvs_receipt_no;?><br>
				<?php endif;?>
				<?php if ($order->cvs_haraikomi_url) : ?>
					払込票URL：<a href="<?php echo $order->cvs_haraikomi_url;?>" target="_blank"><?php echo $order->cvs_haraikomi_url;?></a><br>
				<?php endif;?>
				支払期限：<?php echo $order->cvs_limit_date;?><br>

				店頭でのお支払い方法：<br>
				<?php if ($order->cvs_type == "sej1") : ?>
					<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/711_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/711_m.html</a><br>
				<?php elseif ($order->cvs_type == "econ1") : ?>
					<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/lawson_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/lawson_m.html</a><br>
				<?php elseif ($order->cvs_type == "econ2") : ?>
					<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/famima2_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/famima2_m.html</a><br>
				<?php elseif ($order->cvs_type == "econ3") : ?>
					<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/ministop_loppi_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/ministop_loppi_m.html</a><br>
				<?php elseif ($order->cvs_type == "econ4") : ?>
					<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/seicomart_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/seicomart_m.html</a><br>
				<?php elseif ($order->cvs_type == "other1") : ?>
					<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/circleksunkus_econ_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/circleksunkus_econ_m.html</a><br>
				<?php elseif ($order->cvs_type == "other2") : ?>
					<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/dailyamazaki_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/dailyamazaki_m.html</a><br>
				<?php endif;?>
				<br>
				<br>
				※上記期限までにお支払いいただけなかった場合、ご注文はキャンセルとさせていただきます。<br>
			<?php endif;?>
			<br />
			■商品受取方法<br />
			下記をご用意いただき、選択受取日時に「VISUAL JAPAN SUMMIT 2016」会場受取窓口で提示してください。<br />
			下記をお持ちでない場合、商品をお渡しすることはできません。忘れずに必ずお持ちください。<br />
			<br />
			①ご本人確認書類(運転免許証、健康保険証、学生証など)<br />
			②上記受注番号下5桁<br />
			※商品受取に「VISUAL JAPAN SUMMIT 2016」入場チケットは必要ありません。<br />
			<br />
			・会場内受取窓口：受取当日会場にて告知予定<br />
			・選択受取日時以外での商品受取はできません。必ず選択した時間内にお受け取りください。<br />
			・会場でのお受け取りは、ご注文されたご本人様のみに限らせていただきます。<br />
			・会場受取の方は、グッズ販売列に並ぶ必要はありません。選択時間内に会場受取窓口に直接お越し下さい。<br />
			<br />
			■注意事項<br />
			・ご注文は、お1人様1回のみとなります。<br />
			・注文確定後の受取日時変更はできません。<br />

			<?php if ($order->payment == 1) : ?>
				<br />
				※クレジットカード請求明細の請求屋号は「official-goods-store.jp」になります。<br /><br />
			<?php endif;?>

			<?php if ($order->payment == 4) : ?>
				※商品発送後、別途請求書が郵送されます。請求書到着後14日以内にお支払いください。<br /><br />
			<?php endif;?>

		</p>
	</div>

	<div class="notice">
		<strong>※注文確認メールが届かない場合</strong>
		<p class="description">
			注文確認メールが届かない場合でも、正常にご注文をお受けさせていただいております。<br />
		</p>
		<strong>※注文確認メールが届かない原因</strong>
		<ul>
			<li>( ! ) メールアドレス間違い</li>
			<li>( ! ) 迷惑メールボックスに注文確認メールが入っている</li>
			<li>( ! ) スマートフォンやフィーチャーフォンをご利用の場合、「PCからのメールを受信しない」「なりすまし設定を有効にしている」等のメール受信設定に引っかかっている（ドメイン指定受信設定されている方は「**@official-goods-store.jp」からのメールを受信できるようにしてください。）</li>
		</ul>
		<?php if ($order->payment == 4) : ?>
			<br />
			<strong>※代金のお支払いについて</strong>
			<ul>
				<li>・当店にかわり、後払い.com運営会社の(株)キャッチボールより請求書が送られます。</li>
				<li>・商品到着と請求書の到着は別になります。</li>
				<li>・請求書発行から14日後までにお支払い下さい。</li>
				<li>・銀行 / 郵便局 / コンビニでお支払いいただけます。</li>
				<?php if ($order->result != 1) : ?>
					<li>・お届け先の住所 / 電話番号を間違って登録してしまった場合、「後払い.com」の審査が通らないことがあります。その場合は別途メールにてご連絡いたしますので、メール内容ご確認ください。</li>
				<?php endif;?>
			</ul>
		<?php endif;?>
		<br />
	</div>
</div>



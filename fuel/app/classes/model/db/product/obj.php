<?php
class Model_Db_Product_Obj extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'type',
		'product_id',
		'obj_url',
		'sort',
		'status',
		'insert_date',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_product_obj';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		return $val;
	}

	public static function findList($product_id , $type )
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= " FROM " . self::$_table_name;
		$sql .= " WHERE 1 = 1 ";
		$sql .= "AND product_id = ".$product_id." ";
		$sql .= "AND type = '".$type."' ";
		$sql .= "AND status >= 0 ";
		$sql .= " ORDER BY sort ";

		$query = DB::query($sql);
		$data = $query->execute()->as_array();

		return $data;
	}

	public static function findCount($product_id , $type )
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " count(*) as cnt";
		$sql .= " FROM " . self::$_table_name;
		$sql .= " WHERE 1 = 1 ";
		$sql .= "AND product_id = ".$product_id." ";
		$sql .= "AND type = '".$type."' ";

		$query = DB::query($sql);
		$data = $query->execute()->current();

		return $data;
	}

	public static function findTopImageUrl($product_id , $type="pc" )
	{
		$sql = "";
		$sql .= " SELECT obj_url ";
		$sql .= " FROM ". self::$_table_name;
		$sql .= " WHERE 1 =1 ";
		$sql .= " AND product_id = ".$product_id." ";
		$sql .= " AND TYPE = '".$type."' ";
		$sql .= " AND STATUS = 1 ";
		$sql .= " ORDER BY sort ASC  ";
		$sql .= " LIMIT 1 ";
		$query = DB::query($sql);
		$data = $query->execute()->current();

		return $data;
	}

	public static function findImageListUrl($product_id , $type="pc" )
	{
		$sql = "";
		$sql .= " SELECT obj_url ";
		$sql .= " FROM ". self::$_table_name;
		$sql .= " WHERE 1 =1 ";
		$sql .= " AND product_id = ".$product_id." ";
		$sql .= " AND TYPE = '".$type."' ";
		$sql .= " AND STATUS = 1 ";
		$sql .= " ORDER BY sort ASC  ";
		$query = DB::query($sql);
		$data = $query->execute()->as_array();

		return $data;
	}

}


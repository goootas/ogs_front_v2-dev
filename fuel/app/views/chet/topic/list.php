<div class="TOPICS">
	<div class="CONTAINER">
		<div class="content-head">
			<div class="content-headline">
				<h1>TOPICS</h1>
				<hr>
			</div>
		</div>
		<div class="content-body">
			<ul class="list">
				<?php if ($topics): ?>
				<?php foreach ($topics as $key => $line): ?>
				<li>
					<?php $tag = '<dl><dt>'.$line["view_date"].'</dt><dd class="">'.$line["title"].'</dd></dl>'; ?>
					<?php echo Html::anchor('/topic/detail/'.$line["id"], $tag, array("class" => "innner")) ?>
				</li>
				<?php endforeach; ?>
				<?php else: ?>
				<?php endif; ?>
			</ul>

			<div class="PAGER">
				<ul>
					<?php foreach ($paginations as $key => $val) {
						if($val["type"] == "previous" || $val["type"] == "previous-inactive"){
							echo '<li class="arrow left"><a href="'.$val["uri"].'"><img src="/assets/img/chet/ICON_pager_left.png" /></a></li>';
						}
						if($val["type"] == "next" || $val["type"] == "next-inactive"){
							echo '<li class="arrow left"><a href="'.$val["uri"].'"><img src="/assets/img/chet/ICON_pager_right.png" /></a></li>';
						}
						if($val["type"] == "active" || $val["type"] == "regular"){
							if($val["type"] == "active"){
								echo '<li class="current"><span>'.$val["title"].'</span></li>';
							}else{
								echo '<li><a href="'.$val["uri"].'"><span>'.$val["title"].'</span></li>';
							}
						}
					} ?>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php if ($products): ?>
	<?php foreach ($products as $line): ?>
		<hr>
		<?php
		$img_html = "";
		if(isset($line["imgs"][0])){
			$img_html = '<img src="/'.$shop_data["dir"].'/fpimg?t=1&img=http://'.$shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0].'" align="left"  width="100" height="100">';
		}
//		$caption_html = '<br>'.$line["code"].'<br>¥'.number_format(floor($line["price_sale"] * $tax)).'(税込)';
		$caption_html = '<br>¥'.number_format(floor($line["price_sale"] * $tax)).'(税込)';
		?>
		<table width="100%">
			<tr>
				<td width="100">
					<?php echo $img_html;?>
				</td>
				<td valign="top">
					<?php echo Html::anchor('/product/detail/'.$line["id"].$session_get_param, "<font color='#000000' style='font-size:x-small;'>".$line["title"]."</font>") ?>
					<font style='font-size:x-small;'><?php echo $caption_html;?></font>
				</td>
			</tr>
		</table>
	<?php endforeach; ?>
	<hr>

	<?php if(Pagination::instance('list')->total_pages): ?>
		<br>
		<div align="center">
			<?php if(isset($paginations["previous"]) && $paginations["previous"]["uri"] !="#"): ?>
				<a href="<?php echo $paginations["previous"]["uri"].$session_get_param;?>"><font color='#EA6900'>←戻る</font></a>
			<?php else : ?>
				&nbsp;&nbsp;
			<?php endif ; ?>
			<?php if(isset($paginations["next"]) && $paginations["next"]["uri"] !="#"): ?>
				<a href="<?php echo $paginations["next"]["uri"].$session_get_param;?>"><font color='#EA6900'>次へ→</font></a>
			<?php else : ?>
				&nbsp;&nbsp;
			<?php endif ; ?>
		</div>
	<?php endif; ?>

<?php endif; ?>



<?php
use Fuel\Core\DB;
use Fuel\Core\Log;

/**
 * マイページ
 */
class Controller_Mypage extends Controller_Basefront
{
	public function before()
	{
		// ログイン認証
		parent::before();
		is_null($this->user_data) and Response::redirect('/login');
	}

	/*
	 * マイページ
	 */
	public function action_index()
	{
		//購入履歴取得
		$this->template->set_global('user', $this->user_data, false);

		$sql = "select b.shop_id,b.user_id,b.order_id, b.postage,b.fee,b.insert_date,sum(d.price) as d_total " .
			" from tbl_order_detail d left join tbl_order b on d.order_id = b.order_id " .
			" where b.user_id = " . $this->user_data->id .
			" group by order_id " .
			" order by b.insert_date desc ";

		$orders = DB::query($sql)->execute()->as_array();
		$this->template->set_global('order', $orders, false);

		$this->template->title   = 'マイページ';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/mypage/index');
	}

	/*
	 * 基本情報変更
	 */
	public function action_profile()
	{
		if(!$user = Model_Db_User::find($this->user_data->id))
		{
			Session::set_flash('error', 'Could not find user #' . $this->user_data->id);
			Response::redirect('/login');
		}

		if(Input::method() == 'POST')
		{
			$chkMail = Model_Db_User::findEmail3($user->shop_id,$user->id,Input::post('email'));
			if($chkMail){
				Session::set_flash('error', "入力されたメールアドレスは既に会員登録されています。<br>");
			}else{
				$val = Model_Db_User::validate('regist2');
				if($val->run())
				{
					$old_email = $user->email;
					$user->email			= Input::post('email');
					$user->password			= Input::post('password');
					$user->username_sei		= Input::post('username_sei');
					$user->username_mei		= Input::post('username_mei');
					$user->username_sei_kana	= Input::post('username_sei_kana');
					$user->username_mei_kana	= Input::post('username_mei_kana');
					$user->tel1				= Input::post('tel1');
					$user->tel2				= Input::post('tel2');
					$user->tel3				= Input::post('tel3');
					$user->zip				= Input::post('zip');
					$user->prefecture		= Input::post('state');
					$user->address1			= Input::post('address1');
					$user->address2			= Input::post('address2');
					$user->birthday 		= Input::post('birthday');
					$user->sex      		= Input::post('sex');
					$user->mailmagazine		= Input::post('mailmagazine');
					$user->funclub_flg		= Input::post('funclub_flg',NULL);

					if($user->save())
					{
						if(isset($this->shop_data["mailchimp_apikey"]) && isset($this->shop_data["mailchimp_listid"]))
						{
							// メアドの変更があった場合は、古いアドレスを解除する
							if($old_email != $user->email){
								Model_Lib_MailChimp::unsubscribed($this->shop_data["mailchimp_apikey"],$this->shop_data["mailchimp_listid"],
									$old_email,$user->username_mei,$user->username_sei);
							}
							if($user->mailmagazine == "1")
							{
								//メルマガ登録
								Model_Lib_MailChimp::subscribed($this->shop_data["mailchimp_apikey"],$this->shop_data["mailchimp_listid"],
									$user->email,$user->username_mei,$user->username_sei);
							}else
							{
								//メルマガ解除
								Model_Lib_MailChimp::unsubscribed($this->shop_data["mailchimp_apikey"],$this->shop_data["mailchimp_listid"],
									$user->email,$user->username_mei,$user->username_sei);
							}
						}

						$this->user_data = Model_Db_User::find($this->user_data->id);
						Cache::set("user_data_".$this->user_data->id, $this->user_data, 60 * 5); // 5分キャッシュ

						Response::redirect('/mypage'.$this->session_get_param);
					} else
					{
						Session::set_flash('error', 'Could not update user #' . $this->user_data->id);
					}
				} else
				{
					if(Input::method() == 'POST')
					{
						$user->email = $val->validated('email');
						$user->status = $val->validated('status');
						Session::set_flash('error', $val->error());
					}
				}
			}
		}
		$this->template->set_global('user', $user, false);
		$this->template->title   = "基本情報変更";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/mypage/profile/input');
	}

	/*
	 * 退会
	 */
	public function action_leave()
	{
		$user = Model_Db_User::find($this->user_data->id);
		if($user)
		{
			// 論理削除
			$user->status = Config::get('status_value.deleted');
			$user->save();
			if(isset($this->shop_data["mailchimp_apikey"]) && isset($this->shop_data["mailchimp_listid"]))
			{
				//メルマガ解除
				Model_Lib_MailChimp::unsubscribed($this->shop_data["mailchimp_apikey"],$this->shop_data["mailchimp_listid"],
					$user->email,$user->username_mei,$user->username_sei);
			}
		}
		//セッションクリア
		Session::delete("user");
		Response::redirect("/info/leave");
	}

}

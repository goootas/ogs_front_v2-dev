<?php
class Model_Db_Order extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'order_id',
        'shop_id',
        'user_id',
        'order_username_sei',
        'order_username_mei',
        'order_username_sei_kana',
        'order_username_mei_kana',
        'order_tel1',
        'order_tel2',
        'order_tel3',
        'order_email',
        'order_zip',
        'order_prefecture',
        'order_address1',
        'order_address2',
        'order_sex',
        'order_birthday',
        'order_funclub_flg',
        'order_event_flg',
        'order_type',
        'deliver_username_sei',
        'deliver_username_mei',
        'deliver_tel1',
        'deliver_tel2',
        'deliver_tel3',
        'deliver_zip',
        'deliver_prefecture',
        'deliver_address1',
        'deliver_address2',
        'deliver_day',
        'deliver_time',
        'comment',
        'postage',
        'fee',
        'paid_point',
        'earn_point',
        'atobarai_code',
        'atobarai_invoice_flg',
        'transport_id',
        'tracking_number',
        'capture_date',
        'delivery_date',
        'cancel_date',
        'payment',
        'cvs_type',
        'cvs_receipt_no',
        'cvs_haraikomi_url',
        'cvs_limit_date',
        'status',
        'result',
        'insert_date',
        'update_id',
        'update_date',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => true,
            'property' => 'insert_date',
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => true,
            'property' => 'update_date',
        ),
    );
    protected static $_table_name = 'tbl_order';

    protected static $_has_many = array(
        'detail' => array(
            'key_from' => 'order_id',
            'model_to' => 'Model_Db_Order_Detail',
            'key_to' => 'order_id',
            'cascade_save' => false,
            'cascade_delete' => false,
            )
        );

    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        return $val;
    }

	//注文履歴を取得
	// ユーザーIDに紐づく購入履歴を取得
	//	public static function findListFront($where="",$sort="")
	public static function findListFront($where="")
	{
		$data = static::find('first', array(
				'related' => array('detail'),
				'where' => $where,
//				'order_by' => $sort,
			)
		);
		return $data;
	}
	//注文履歴を取得
	public static function findDetailFront($where="")
	{
		$data = static::find('first', array(
				'where' => $where,
			)
		);
		return $data;
	}

	//注文履歴を取得
	public static function updateUserID($order_id , $user_id)
	{
		if(!$order_id || !$user_id){
			return;
		}

		$sql = "UPDATE ".self::$_table_name." SET user_id = :user_id WHERE order_id = :order_id ";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'order_id' => $order_id,
				'user_id' => $user_id,
			)
		);
		$query->execute();
		return;
	}

}

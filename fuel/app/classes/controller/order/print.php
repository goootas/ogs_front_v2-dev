<?php
use Fuel\Core\Config;
use Fuel\Core\Log;
use Fuel\Core\Session;
use Fuel\Core\Request;
use Fuel\Core\Response;
use Fuel\Core\Input;
use Fuel\Core\View;

/*
 * 写真集名入れ
 *
 * 該当テーブル
 * > ファンクラグマスタ
 *  管理項目
 *   シーケンス
 *   店舗ID
 *   ファンクラグ会員番号
 *   お名前（姓）
 *   お名前（名）
 *   電話番号
 *   LAST VISUALIVE 参加公演
 *   席番号
 *
 * > 購入紐付けテーブル
 *  管理項目
 *   注文ID
 *   ファンクラグマスタID
 */
class Controller_Order_Print extends Controller_Basefront
{
	/*
	 * カート内容確認
	 *
	 * 該当商品がない場合は、プリント用セッションをクリアして注文者情報入力画面へリダイレクト
	 *
	 */
	public function before()
	{
		parent::before();
		$this->template->title = "";
		if(! Session::get("cart") &&
			! in_array(Request::active()->action, array('finish'))){
			Response::redirect('/cart'.$this->session_get_param);
		}
		// 該当商品がない場合は、注文者情報入力画面へリダイレクト
		$target_product_detail_ids = Config::get("print_product_detail_ids");
		$carts = Session::get("cart");
		$hit_cnt = 0;
		$target_cnt = 0;// 名入れできる商品数
		foreach ($carts as $cart) {
			foreach ($target_product_detail_ids as $target_product_detail_id) {
				if($cart["did"] == $target_product_detail_id){
					$target_cnt = $cart["num"];
					$hit_cnt++;
				}
			}
		}

		if($hit_cnt == 0){
			Session::delete("print");
			Response::redirect('/order/input1'.$this->session_get_param);
		}
		$this->template->set_global('target_cnt', $target_cnt, false);
	}

	/*
	 * 会員情報入力
	 */
	public function action_member($change=0)
	{
		$club_data = Session::get("print.club_data");
		if (Input::method() == 'POST') {
			$club_data["club_no"]	= Input::post("club_no");
//			$club_data["name_sei"]	= Input::post("name_sei");
//			$club_data["name_mei"]	= Input::post("name_mei");
			$club_data["tel"]		= Input::post("tel");
//			$print_data = Model_Db_Print::findDetail($this->shop_data["id"],$club_data["club_no"], $club_data["name_sei"] ,$club_data["name_mei"] , $club_data["tel"]);
			$print_data = Model_Db_Print::findDetail($this->shop_data["id"],$club_data["club_no"], $club_data["tel"]);
			if($print_data){
				$print_data2 = Model_Db_Print::findDetail2($this->shop_data["id"],$club_data["club_no"]);
				Session::set("print.club_data",$club_data);
				Session::set("print.print_data",$print_data2);

				Response::redirect('/order/print/select'.$this->session_get_param);
			}
			Session::set_flash('error', '入力内容に誤りがあるようです。内容ご確認後再度入力お願いいたします。');
		}
		$this->template->set_global('change', $change, false);
		$this->template->set_global('club_data', $club_data, false);
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/order/print/member');
	}

	/*
	 * 名入れ選択
	 * 別商品IDには対応していない※同一商品IDで複数購入はOK
	 */
	public function action_select($change=0)
	{
		$club_data = Session::get("print.club_data");
		$print_data = Session::get("print.print_data");// 選択可能情報
		$select_ids = Session::get("print.select_ids");// 選択した情報

		if (Input::method() == 'POST') {
			Session::delete("print.select_ids");
			$select_ids	= Input::post("select_ids");

			// 入力チェック（ガラ用）
			// 該当すべて入力済か確認する。
			// 未設定がある場合は、エラーメッセージを表示する
			$error_cnt = 0;
			foreach ($select_ids as $id) {
				if(!$id){
					$error_cnt++;
				}
			}

			if($error_cnt){
				Session::set_flash('error', '必須項目です');
			}else{
				Session::set("print.select_ids",$select_ids);
				if(Input::post("change_flg")){
					Response::redirect('/order/confirm'.$this->session_get_param);
				}
				Response::redirect('/order/input1'.$this->session_get_param);
			}
		}

		$form_data = array();
		foreach ($print_data as $val){
			$form_data[$val["id"]] = $val["param1"]."/".$val["param2"];
		}
		foreach ($print_data as $val){
			$form_name_data = $val["name_mei"]." ".$val["name_sei"];
		}

		$this->template->set_global('change', $change, false);
		$this->template->set_global('club_data', $club_data, false);
		$this->template->set_global('print_data', $print_data, false);
		$this->template->set_global('form_data', $form_data, false);
		$this->template->set_global('form_name_data', $form_name_data, false);
		$this->template->set_global('select_ids', $select_ids, false);

		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/order/print/select');
	}
}

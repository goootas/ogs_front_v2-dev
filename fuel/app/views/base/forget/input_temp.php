<div class="panel forget">
    <h4 class="subheadline">パスワード再設定</h4>
	<div class="description">
        <p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
        <p>
            パスワード再設定用URLを送付いたします。<br>
            お客様のメールアドレスを入力して、送信ボタンを押してください。<br>
        </p>
	</div>

	<?php echo Form::open(array('id' => 'forget' ,'class'=>'form-inline' ,'autocomplete'=>'off' ));?>
    <div class="form-group">
		<?php echo Form::label('メールアドレス'); ?>
		<div class="form-container">
            <div class="inner">
            <?php echo Form::input('email', Input::post('email', ''),
                array('class' => 'validate[required,custom[email]] form-control' ,'placeholder' =>'XXXXXXX@XXXXX.COM','size' => "60%")); ?>
            </div>
		</div>
    </div>
	<div class="buttons">
        <?php echo Form::submit('exec', '送信', array('class' => 'button rect key size-L')); ?>
    </div>
    <?php echo Form::close();?>
	<div class="description notice">通販サイトリニューアルに伴い、2015年6月以前に会員登録されたお客様は、大変お手数ですが、再度新規会員登録をお願いいたします。</div>
</div>



<script>
	$(function(){
		$("#forget").validationEngine({promptPosition : "topLeft"});
	});
</script>

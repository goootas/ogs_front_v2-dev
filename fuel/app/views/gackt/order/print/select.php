
<style>
	.CONTENTS {
		margin-top: 0px;
	}
</style>

<p style="color: #FF0000;text-align: center"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<?php echo Form::open(array('id' => "print" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<div class="forms">
	<!--	<h4 class="headline">お客様情報</h4>-->
	<div style="text-align: center">
		刻印される内容をご確認ください。
	</div>

	<div class="fixed-data">
		<table>
			<tr>
				<td class="head">G&LOVERS会員番号</td>
				<td class="body"><?php echo $club_data["club_no"]?></td>
			</tr>
<!--			<tr>-->
<!--				<td class="head">お名前</td>-->
<!--				<td class="body">--><?php //echo $club_data["name_mei"]." ".$club_data["name_sei"]?><!-- 様</td>-->
<!--			</tr>-->
			<?php for ($i=0;$i < $target_cnt;$i++):?>
				<?php if($target_cnt > 1):?>
					<tr>
						<td class="head" colspan="2">商品<?PHP echo $i+1;?></td>
					</tr>
				<?php endif;?>

				<tr>
					<td class="head">お名前</td>
					<td class="body">
						<?php echo $form_name_data;?>
						<div class="description notice">
							アルファベットの綴りに誤りがある場合は、大変お手数ですがご注文確定後、CONTACTページより正しい綴りをご連絡ください。(刻印するお名前の変更はできません)
						</div>
					</td>
				</tr>
				<tr>
					<td class="head">参加公演/席番号<span class="caption" style="color: #FF0000">※選択必須</span></td>
					<td class="body">
						<?php foreach ($form_data as $key => $value) : ?>
							<input type="radio" name="select_ids[<?php echo $i;?>]" value="<?php echo $key;?>" class="validate[required]" <?php echo isset($select_ids[$i]) && $select_ids[$i] == $key ? "checked" : ""?>><?php echo $value;?><br>
						<?php endforeach;?>
						<div class="description notice">
							※商品1点に複数公演名プリント不可<br>
							※注文確定後変更不可
						</div>
					</td>
				</tr>
			<?php endfor;?>
		</table>
	</div>

	<div class="buttons count-2">
		<?php echo Html::anchor('/cart', 'カートに戻る',array('class' => 'button rect size-L'),true); ?>
		<?php if( $change == 1) :?>
			<?php echo Html::anchor('#', '変更',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php else:?>
			<?php echo Html::anchor('#', '次へ進む',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php endif;?>
	</div>
</div>
<?php echo Form::hidden('change_flg',$change); ?>

<?php echo Form::close();?>

<script type="text/javascript">
	$(function(){
		$("[id=next]").click(function(){
			$("#print").submit();
		});
		$("#print").validationEngine({
			promptPosition : "topLeft",
			scroll: false,
		});
	});
</script>

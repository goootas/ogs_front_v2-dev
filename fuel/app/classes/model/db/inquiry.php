<?php
class Model_Db_Inquiry extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'user_id',
		'username',
		'email',
		'message',
		'answer',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_inquiry';

	protected static $_has_one = array(
		'user' => array(
			'key_from' => 'user_id',
			'model_to' => 'Model_Db_User',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
			),
		'shop' => array(
			'key_from' => 'shop_id',
			'model_to' => 'Model_Db_Shop',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
			),
		);

	public static function validate($factory)
    {
        $val = Validation::forge($factory);
//		$val->add('username', 'お名前')->add_rule('required');
		$val->add('email', 'メールアドレス')->add_rule('required')
			->add_rule('valid_email');
		$val->add('message', 'お問い合わせ内容')->add_rule('required');

        return $val;
    }

	public static function findListTool($where="",$sort="")
	{
		$data = static::find('all', array(
			'related'	=> array('shop','user'),
			'where'		=> $where,
			'order_by'	=> $sort,
			)
		);
		return $data;
	}
	public static function findDetailTool($where="")
	{
		$data = static::find('first', array(
			'related'	=> array('shop','user'),
			'where'		=> $where,
			)
		);
		return $data;
	}
}

<?php
namespace Model\lib;

use Fuel\Core\Config;
use Fuel\Core\Log;

class Order extends \Model
{
	/*
	 * 注文情報登録（決済ステータス含まない）
	 */
	public static function setOrderInsert($order,$shop_id,$user_id="")
	{
		// 共通設定ファイル読み込み
		Config::load("common");

		$order_type = Term::chkCartType($order["products"]);

		$order_req = \Model_Db_Order::forge();
		$order_req->order_id				= $order["order_id"];
		$order_req->shop_id					= $shop_id;
		$order_req->user_id					= $user_id;
		$order_req->order_username_sei		= trim($order["order_username_sei"]);
		$order_req->order_username_mei		= trim($order["order_username_mei"]);
		$order_req->order_username_sei_kana	= trim($order["order_username_sei_kana"]);
		$order_req->order_username_mei_kana	= trim($order["order_username_mei_kana"]);
		$order_req->order_email				= trim($order["order_email"]);
		$order_req->order_tel1				= trim($order["order_tel1"]);
		$order_req->order_tel2				= trim($order["order_tel2"]);
		$order_req->order_tel3				= trim($order["order_tel3"]);
		$order_req->order_zip				= $order["order_zip"];
		$order_req->order_prefecture		= $order["order_state"];

		$wk_address1 = str_replace("–","-",$order["order_address1"]);
		$wk_address1 = str_replace($order["order_state"],"",$wk_address1);
		$order_req->order_address1			= mb_convert_kana($wk_address1, "ASV");
		$order_req->order_address2			= mb_convert_kana(str_replace("–","-",$order["order_address2"]), "ASV");

		$order_req->order_sex				= !empty($order["order_sex"]) ? $order["order_sex"] :"" ;
		$order_req->order_birthday			= $order["order_birthday"];
		$order_req->order_funclub_flg		= !empty($order["order_funclub_flg"]) ? $order["order_funclub_flg"] : NULL ;
		$order_req->order_event_flg			= !empty($order["order_event_flg"]) ? $order["order_event_flg"] : 0 ;
		$order_req->order_type				= !empty($order_type) ? $order_type : 0 ;
		$order_req->deliver_username_sei	= trim($order["deliver_username_sei"]);
		$order_req->deliver_username_mei	= trim($order["deliver_username_mei"]);
		$order_req->deliver_tel1			= trim($order["deliver_tel1"]);
		$order_req->deliver_tel2			= trim($order["deliver_tel2"]);
		$order_req->deliver_tel3			= trim($order["deliver_tel3"]);
		$order_req->deliver_zip				= trim($order["deliver_zip"]);
		$order_req->deliver_prefecture		= $order["deliver_state"];

		$wk_address1 = str_replace("–","-",$order["deliver_address1"]);
		$wk_address1 = str_replace($order["deliver_state"],"",$wk_address1);

		$order_req->deliver_address1		= mb_convert_kana($wk_address1, "ASV");
		$order_req->deliver_address2		= mb_convert_kana(str_replace("–","-",$order["deliver_address2"]), "ASV");

		$order_req->deliver_day				= isset($order["delivery_date"]) ? $order["delivery_date"] : "" ;
		$order_req->deliver_time			= isset($order["delivery_time"]) ? $order["delivery_time"] : "";
//		$order_req->comment					= $order["delivery_comment"] ? $order["delivery_comment"] : "";
		$order_req->comment					= "";
		$order_req->postage					= $order["postage"];
		$order_req->fee						= $order["fee"];
		$order_req->capture_date			= ($order["payment"] == 1 || $order["payment"] == 4 || $order["payment"] == 5) ? date("Y-m-d H:i:s") :NULL;
		$order_req->payment					= $order["payment"];
		$order_req->cvs_type				= isset($order["cvs"]) ? $order["cvs"] : "";

		if($order["payment"] == 2){
			$order_req->cvs_limit_date			= isset($order["limit_date"]) ? $order["limit_date"] : "";
		}else{
			$order_req->cvs_limit_date			= "";
		}

		// 後払い請求書送付フラグ（0:別送 1:同梱）
		$deliver_address		= trim($order["deliver_zip"]) .$order["deliver_state"] .str_replace("–","-",$order["deliver_address1"]) .
			str_replace($order["deliver_state"],"",$wk_address1).str_replace("–","-",$order["deliver_address2"]);
		$order_address	= trim($order["order_zip"]) .$order["order_state"] .str_replace("–","-",$order["order_address1"]) .
			str_replace($order["order_state"],"",$wk_address1).str_replace("–","-",$order["order_address2"]);
		if(trim($deliver_address) == trim($order_address)){
			$order_req->atobarai_invoice_flg = 1;
		}else{
			$order_req->atobarai_invoice_flg = 0;
		}

		foreach ($order["products"] as $value) {
			$order_req->detail[] = \Model_Db_Order_Detail::forge(array(
				'order_id'  => $order["order_id"],
				'did'       => $value["did"],
				'num'       => $value["num"],
				'price'     => $value["price"],
				'status'    => Config::get("status_value.enable"),
			));
		}
		$order_req and $order_req->save();

		return $order_req;
	}

	/*
	 * 注文情報更新（決済API実行後の伝票更新作業）
	 */
	public static function setOrderUpdate($order,$res)
	{
		// 共通設定ファイル読み込み
		Config::load("common");

		$order_req = \Model_Db_Order::find($order->id);

		switch($order_req->payment){
			case Config::get("payment.value.card"):
				$order_req->status		= Config::get("order.status.value.authorize");
				$order_req->result		= Config::get("order.result.value.".$res["status"]);
				break;
			case Config::get("payment.value.cvs"):
				$order_req->cvs_receipt_no		= $res ? (isset($res["receipt_no"]) ? $res["receipt_no"] : "") : "";
				$order_req->cvs_haraikomi_url	= $res ? (isset($res["haraikomi_url"]) ? $res["haraikomi_url"] : "") : "";
				$order_req->status				= Config::get("order.status.value.publish");
				$order_req->result				= Config::get("order.result.value.".$res["status"]);
				break;
			case Config::get("payment.value.cod"):
				$order_req->status		= Config::get("order.status.value.cod");
//				$order_req->result		= Config::get("order.result.value.".$res["status"]);
				break;
			case Config::get("payment.value.ato"):
				$order_req->atobarai_code	= $res["order_id"];
				$order_req->status			= Config::get("order.status.value.ato");
				$order_req->result			= $res["status"];
				break;
			case Config::get("payment.value.npato"):
				$order_req->result			= 0;// 0:与信中
				break;
			case Config::get("payment.value.rakuten"):
				$order_req->result			= 1;// 1:与信OK
				break;
		}

		$order_req and $order_req->save();
		return $order_req;
	}

	/*
	 * 注文情報登録（名入れ）
	 */
	public static function setOrderNameInsert($order_id,$sess_names)
	{
		// 共通設定ファイル読み込み
		$ses_name_did	= $sess_names["names_did"];
		$ses_name_value	= $sess_names["names_value"];

		foreach ($ses_name_did as $key => $data){

			$order_req = \Model_Db_Order_Name::forge();
			$order_req->order_id	= $order_id;
			$order_req->did			= $ses_name_did[$key];
			$order_req->name		= $ses_name_value[$key];
			$order_req and $order_req->save();

		}

		return ;
	}

	/*
	 * 注文情報登録（現地受渡）
	 */
	public static function setOrderReceivesInsert($order_id,$timetable_id)
	{
		$order = \Model_Db_Receives_Order::forge();
		$order->order_id		= $order_id;
		$order->timetable_id	= $timetable_id;
		$order and $order->save();

		return ;
	}

	/*
	 * 注文情報登録（写真集プリント）
	 */
	public static function setOrderPrintInsert($order_id,$product_id,$product_detail_id,$print_id)
	{
		$print = \Model_Db_Order_Print::forge();
		$print->order_id			= $order_id;
		$print->product_id			= $product_id;
		$print->product_detail_id	= $product_detail_id;
		$print->print_id			= $print_id;
		$print and $print->save();

		return ;
	}



}
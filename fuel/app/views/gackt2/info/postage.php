<h4 class="headline">配送料について</h4>
<div class="postage">
    <?php if ($postage): ?>
        <table>
            <?php foreach ($postage as $base): ?>
                <tr>
                    <td class="head"><?php echo $base->prefectures->name; ?></td>
                    <td class="body">¥<?php echo number_format($base->price); ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php else: ?>
    <?php endif; ?>
    <div class="col-xs-12">
        <hr>
        ※以下の郵便番号への配送は、離島料金が適用されます。
    </div>
    <?php if ($rito): ?>
        <table>
            <?php foreach ($rito as $base): ?>
                <tr>
                    <td class="head">〒<?php echo substr($base->zip, 0,3)."-".substr($base->zip, 3,4); ?></td>
                    <td class="body">¥<?php echo number_format($base->price); ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php else: ?>
    <?php endif; ?>
</div>
<?php
use Model\lib\Image;

/*
 * TOPページ
 */
class Controller_Topic extends Controller_Basefront
{
	public function action_index()
	{
		self::action_list();
	}

	/*
	 * Topic一覧
	 */
	public function action_list($page=0)
	{
		$wheres["shop_id"] = $this->shop_data["id"];
		$topic_total = Model_Db_Topic::findList($wheres);
		$param = array(
			'pagination_url' => ($this->shop_data["type"] ? "/".$this->shop_data["dir"] : "") . '/topic/list/',
			'total_items' => count($topic_total),
			'per_page' => 30,
			'uri_segment' => 3,
			'current_page' => $page,
			'num_links' => 10,
		);

		$paginations = Pagination::forge('list', $param);
		$topic_list = Model_Db_Topic::findList($wheres, $paginations);
		$this->template->set_global('paginations', $paginations->render(true), false);
		$this->template->set_global('total_pages', Pagination::instance('list')->total_pages, false);
		$this->template->set_global('topics', $topic_list, false);

		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/topic/list');

	}

	/*
	 * Topic詳細
	 */
	public function action_detail($id=0)
	{
		//----------------------------------
		// TOPIC情報取得
		//----------------------------------
		$wheres1["shop_id"] = $this->shop_data["id"];
		$wheres1["id"] = $id;
		$topic_list = Model_Db_Topic::findTopicOne($wheres1);

		$this->template->set_global('topics', $topic_list, false);
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/topic/detail');

		//----------------------------------
		// TOPICアーカイブ取得
		//----------------------------------
		$wheres2["shop_id"] = $this->shop_data["id"];
		$topic_list = Model_Db_Topic::findTopicList($wheres2,5);

		$this->template->set_global('topics_archive', $topic_list, false);
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/topic/detail');
	}

	/*
	 * Topic詳細(ステータス無視)
	 */
	public function action_detail_all($id=0)
	{
		//----------------------------------
		// TOPIC情報取得
		//----------------------------------
		$wheres1["shop_id"] = $this->shop_data["id"];
		$wheres1["id"] = $id;
//		$topic_list = Model_Db_Topic::findTopicOne($wheres1);
		$topic_list = Model_Db_Topic::findTopicOneAll($wheres1);

		$this->template->set_global('topics', $topic_list, false);
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/topic/detail');

		//----------------------------------
		// TOPICアーカイブ取得
		//----------------------------------
		$wheres2["shop_id"] = $this->shop_data["id"];
		$topic_list = Model_Db_Topic::findTopicList($wheres2,5);

		$this->template->set_global('topics_archive', $topic_list, false);
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/topic/detail');
	}

}

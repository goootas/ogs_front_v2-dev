<h4>新規会員登録</h4>
<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<p>
	新規会員仮登録を行います。<br>
	会員登録をするメールアドレスを入力し、送信ボタンをクリックしてください。<br>
	入力したメールアドレス宛に本登録用URLを送信いたします。メール記載のURLへアクセス後、本登録を行ってください。<br>
</p>
<div>
	<?php echo Form::open();?>
	<div>メールアドレス</div>
	<div>
		<?php echo Form::input('email', Input::post('email', ''), array("istyle"=>"3", "format"=>"*x", "MODE"=>"alphabet")); ?>
	</div>

	<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
	<?php echo Form::submit('regist', '送信'); ?>
	<?php echo Form::close();?>

</div>
